/**
 * CoDriverChangeEventEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class CoDriverChangeEventEnum implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CoDriverChangeEventEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _HopOn = "HopOn";
    public static final java.lang.String _HopOff = "HopOff";
    public static final java.lang.String _DriverSwap = "DriverSwap";
    public static final CoDriverChangeEventEnum None = new CoDriverChangeEventEnum(_None);
    public static final CoDriverChangeEventEnum HopOn = new CoDriverChangeEventEnum(_HopOn);
    public static final CoDriverChangeEventEnum HopOff = new CoDriverChangeEventEnum(_HopOff);
    public static final CoDriverChangeEventEnum DriverSwap = new CoDriverChangeEventEnum(_DriverSwap);
    public java.lang.String getValue() { return _value_;}
    public static CoDriverChangeEventEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CoDriverChangeEventEnum enumeration = (CoDriverChangeEventEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CoDriverChangeEventEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CoDriverChangeEventEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriverChangeEventEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
