/**
 * Message.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class Message  implements java.io.Serializable {
    private java.math.BigDecimal messageID;

    private java.lang.String vehicle;

    private long vehicleId;

    private java.lang.String vehicleNumberPlate;

    private java.lang.String text;

    private java.lang.String reference;

    private double latitude;

    private double longitude;

    private SimpexLib.MessageService.Tour tour;

    private SimpexLib.MessageService.Place place;

    private SimpexLib.MessageService.Order order;

    private int form;

    private SimpexLib.MessageService.FmsData fms;

    private boolean fmsValid;

    private SimpexLib.MessageService.AdditionalValues additionalValues;

    private double odometer;

    private double speed;

    private SimpexLib.MessageService.Address locationInformation;

    private SimpexLib.MessageService.User sender;

    private java.lang.String formName;

    private SimpexLib.MessageService.Driver driver;

    private SimpexLib.MessageService.CoDriver[] coDrivers;

    private SimpexLib.MessageService.Attachment attachment;

    private SimpexLib.MessageService.Attachment attachmentFromDispo;

    private SimpexLib.MessageService.Gps gps;

    private SimpexLib.MessageService.AttachedObject[] attachedObjects;

    private java.util.Calendar messageTimeUtc;

    private java.lang.Integer customerId;

    public Message() {
    }

    public Message(
           java.math.BigDecimal messageID,
           java.lang.String vehicle,
           long vehicleId,
           java.lang.String vehicleNumberPlate,
           java.lang.String text,
           java.lang.String reference,
           double latitude,
           double longitude,
           SimpexLib.MessageService.Tour tour,
           SimpexLib.MessageService.Place place,
           SimpexLib.MessageService.Order order,
           int form,
           SimpexLib.MessageService.FmsData fms,
           boolean fmsValid,
           SimpexLib.MessageService.AdditionalValues additionalValues,
           double odometer,
           double speed,
           SimpexLib.MessageService.Address locationInformation,
           SimpexLib.MessageService.User sender,
           java.lang.String formName,
           SimpexLib.MessageService.Driver driver,
           SimpexLib.MessageService.CoDriver[] coDrivers,
           SimpexLib.MessageService.Attachment attachment,
           SimpexLib.MessageService.Attachment attachmentFromDispo,
           SimpexLib.MessageService.Gps gps,
           SimpexLib.MessageService.AttachedObject[] attachedObjects,
           java.util.Calendar messageTimeUtc,
           java.lang.Integer customerId) {
           this.messageID = messageID;
           this.vehicle = vehicle;
           this.vehicleId = vehicleId;
           this.vehicleNumberPlate = vehicleNumberPlate;
           this.text = text;
           this.reference = reference;
           this.latitude = latitude;
           this.longitude = longitude;
           this.tour = tour;
           this.place = place;
           this.order = order;
           this.form = form;
           this.fms = fms;
           this.fmsValid = fmsValid;
           this.additionalValues = additionalValues;
           this.odometer = odometer;
           this.speed = speed;
           this.locationInformation = locationInformation;
           this.sender = sender;
           this.formName = formName;
           this.driver = driver;
           this.coDrivers = coDrivers;
           this.attachment = attachment;
           this.attachmentFromDispo = attachmentFromDispo;
           this.gps = gps;
           this.attachedObjects = attachedObjects;
           this.messageTimeUtc = messageTimeUtc;
           this.customerId = customerId;
    }


    /**
     * Gets the messageID value for this Message.
     * 
     * @return messageID
     */
    public java.math.BigDecimal getMessageID() {
        return messageID;
    }


    /**
     * Sets the messageID value for this Message.
     * 
     * @param messageID
     */
    public void setMessageID(java.math.BigDecimal messageID) {
        this.messageID = messageID;
    }


    /**
     * Gets the vehicle value for this Message.
     * 
     * @return vehicle
     */
    public java.lang.String getVehicle() {
        return vehicle;
    }


    /**
     * Sets the vehicle value for this Message.
     * 
     * @param vehicle
     */
    public void setVehicle(java.lang.String vehicle) {
        this.vehicle = vehicle;
    }


    /**
     * Gets the vehicleId value for this Message.
     * 
     * @return vehicleId
     */
    public long getVehicleId() {
        return vehicleId;
    }


    /**
     * Sets the vehicleId value for this Message.
     * 
     * @param vehicleId
     */
    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }


    /**
     * Gets the vehicleNumberPlate value for this Message.
     * 
     * @return vehicleNumberPlate
     */
    public java.lang.String getVehicleNumberPlate() {
        return vehicleNumberPlate;
    }


    /**
     * Sets the vehicleNumberPlate value for this Message.
     * 
     * @param vehicleNumberPlate
     */
    public void setVehicleNumberPlate(java.lang.String vehicleNumberPlate) {
        this.vehicleNumberPlate = vehicleNumberPlate;
    }


    /**
     * Gets the text value for this Message.
     * 
     * @return text
     */
    public java.lang.String getText() {
        return text;
    }


    /**
     * Sets the text value for this Message.
     * 
     * @param text
     */
    public void setText(java.lang.String text) {
        this.text = text;
    }


    /**
     * Gets the reference value for this Message.
     * 
     * @return reference
     */
    public java.lang.String getReference() {
        return reference;
    }


    /**
     * Sets the reference value for this Message.
     * 
     * @param reference
     */
    public void setReference(java.lang.String reference) {
        this.reference = reference;
    }


    /**
     * Gets the latitude value for this Message.
     * 
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this Message.
     * 
     * @param latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this Message.
     * 
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this Message.
     * 
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the tour value for this Message.
     * 
     * @return tour
     */
    public SimpexLib.MessageService.Tour getTour() {
        return tour;
    }


    /**
     * Sets the tour value for this Message.
     * 
     * @param tour
     */
    public void setTour(SimpexLib.MessageService.Tour tour) {
        this.tour = tour;
    }


    /**
     * Gets the place value for this Message.
     * 
     * @return place
     */
    public SimpexLib.MessageService.Place getPlace() {
        return place;
    }


    /**
     * Sets the place value for this Message.
     * 
     * @param place
     */
    public void setPlace(SimpexLib.MessageService.Place place) {
        this.place = place;
    }


    /**
     * Gets the order value for this Message.
     * 
     * @return order
     */
    public SimpexLib.MessageService.Order getOrder() {
        return order;
    }


    /**
     * Sets the order value for this Message.
     * 
     * @param order
     */
    public void setOrder(SimpexLib.MessageService.Order order) {
        this.order = order;
    }


    /**
     * Gets the form value for this Message.
     * 
     * @return form
     */
    public int getForm() {
        return form;
    }


    /**
     * Sets the form value for this Message.
     * 
     * @param form
     */
    public void setForm(int form) {
        this.form = form;
    }


    /**
     * Gets the fms value for this Message.
     * 
     * @return fms
     */
    public SimpexLib.MessageService.FmsData getFms() {
        return fms;
    }


    /**
     * Sets the fms value for this Message.
     * 
     * @param fms
     */
    public void setFms(SimpexLib.MessageService.FmsData fms) {
        this.fms = fms;
    }


    /**
     * Gets the fmsValid value for this Message.
     * 
     * @return fmsValid
     */
    public boolean isFmsValid() {
        return fmsValid;
    }


    /**
     * Sets the fmsValid value for this Message.
     * 
     * @param fmsValid
     */
    public void setFmsValid(boolean fmsValid) {
        this.fmsValid = fmsValid;
    }


    /**
     * Gets the additionalValues value for this Message.
     * 
     * @return additionalValues
     */
    public SimpexLib.MessageService.AdditionalValues getAdditionalValues() {
        return additionalValues;
    }


    /**
     * Sets the additionalValues value for this Message.
     * 
     * @param additionalValues
     */
    public void setAdditionalValues(SimpexLib.MessageService.AdditionalValues additionalValues) {
        this.additionalValues = additionalValues;
    }


    /**
     * Gets the odometer value for this Message.
     * 
     * @return odometer
     */
    public double getOdometer() {
        return odometer;
    }


    /**
     * Sets the odometer value for this Message.
     * 
     * @param odometer
     */
    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }


    /**
     * Gets the speed value for this Message.
     * 
     * @return speed
     */
    public double getSpeed() {
        return speed;
    }


    /**
     * Sets the speed value for this Message.
     * 
     * @param speed
     */
    public void setSpeed(double speed) {
        this.speed = speed;
    }


    /**
     * Gets the locationInformation value for this Message.
     * 
     * @return locationInformation
     */
    public SimpexLib.MessageService.Address getLocationInformation() {
        return locationInformation;
    }


    /**
     * Sets the locationInformation value for this Message.
     * 
     * @param locationInformation
     */
    public void setLocationInformation(SimpexLib.MessageService.Address locationInformation) {
        this.locationInformation = locationInformation;
    }


    /**
     * Gets the sender value for this Message.
     * 
     * @return sender
     */
    public SimpexLib.MessageService.User getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this Message.
     * 
     * @param sender
     */
    public void setSender(SimpexLib.MessageService.User sender) {
        this.sender = sender;
    }


    /**
     * Gets the formName value for this Message.
     * 
     * @return formName
     */
    public java.lang.String getFormName() {
        return formName;
    }


    /**
     * Sets the formName value for this Message.
     * 
     * @param formName
     */
    public void setFormName(java.lang.String formName) {
        this.formName = formName;
    }


    /**
     * Gets the driver value for this Message.
     * 
     * @return driver
     */
    public SimpexLib.MessageService.Driver getDriver() {
        return driver;
    }


    /**
     * Sets the driver value for this Message.
     * 
     * @param driver
     */
    public void setDriver(SimpexLib.MessageService.Driver driver) {
        this.driver = driver;
    }


    /**
     * Gets the coDrivers value for this Message.
     * 
     * @return coDrivers
     */
    public SimpexLib.MessageService.CoDriver[] getCoDrivers() {
        return coDrivers;
    }


    /**
     * Sets the coDrivers value for this Message.
     * 
     * @param coDrivers
     */
    public void setCoDrivers(SimpexLib.MessageService.CoDriver[] coDrivers) {
        this.coDrivers = coDrivers;
    }


    /**
     * Gets the attachment value for this Message.
     * 
     * @return attachment
     */
    public SimpexLib.MessageService.Attachment getAttachment() {
        return attachment;
    }


    /**
     * Sets the attachment value for this Message.
     * 
     * @param attachment
     */
    public void setAttachment(SimpexLib.MessageService.Attachment attachment) {
        this.attachment = attachment;
    }


    /**
     * Gets the attachmentFromDispo value for this Message.
     * 
     * @return attachmentFromDispo
     */
    public SimpexLib.MessageService.Attachment getAttachmentFromDispo() {
        return attachmentFromDispo;
    }


    /**
     * Sets the attachmentFromDispo value for this Message.
     * 
     * @param attachmentFromDispo
     */
    public void setAttachmentFromDispo(SimpexLib.MessageService.Attachment attachmentFromDispo) {
        this.attachmentFromDispo = attachmentFromDispo;
    }


    /**
     * Gets the gps value for this Message.
     * 
     * @return gps
     */
    public SimpexLib.MessageService.Gps getGps() {
        return gps;
    }


    /**
     * Sets the gps value for this Message.
     * 
     * @param gps
     */
    public void setGps(SimpexLib.MessageService.Gps gps) {
        this.gps = gps;
    }


    /**
     * Gets the attachedObjects value for this Message.
     * 
     * @return attachedObjects
     */
    public SimpexLib.MessageService.AttachedObject[] getAttachedObjects() {
        return attachedObjects;
    }


    /**
     * Sets the attachedObjects value for this Message.
     * 
     * @param attachedObjects
     */
    public void setAttachedObjects(SimpexLib.MessageService.AttachedObject[] attachedObjects) {
        this.attachedObjects = attachedObjects;
    }


    /**
     * Gets the messageTimeUtc value for this Message.
     * 
     * @return messageTimeUtc
     */
    public java.util.Calendar getMessageTimeUtc() {
        return messageTimeUtc;
    }


    /**
     * Sets the messageTimeUtc value for this Message.
     * 
     * @param messageTimeUtc
     */
    public void setMessageTimeUtc(java.util.Calendar messageTimeUtc) {
        this.messageTimeUtc = messageTimeUtc;
    }


    /**
     * Gets the customerId value for this Message.
     * 
     * @return customerId
     */
    public java.lang.Integer getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this Message.
     * 
     * @param customerId
     */
    public void setCustomerId(java.lang.Integer customerId) {
        this.customerId = customerId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Message)) return false;
        Message other = (Message) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.messageID==null && other.getMessageID()==null) || 
             (this.messageID!=null &&
              this.messageID.equals(other.getMessageID()))) &&
            ((this.vehicle==null && other.getVehicle()==null) || 
             (this.vehicle!=null &&
              this.vehicle.equals(other.getVehicle()))) &&
            this.vehicleId == other.getVehicleId() &&
            ((this.vehicleNumberPlate==null && other.getVehicleNumberPlate()==null) || 
             (this.vehicleNumberPlate!=null &&
              this.vehicleNumberPlate.equals(other.getVehicleNumberPlate()))) &&
            ((this.text==null && other.getText()==null) || 
             (this.text!=null &&
              this.text.equals(other.getText()))) &&
            ((this.reference==null && other.getReference()==null) || 
             (this.reference!=null &&
              this.reference.equals(other.getReference()))) &&
            this.latitude == other.getLatitude() &&
            this.longitude == other.getLongitude() &&
            ((this.tour==null && other.getTour()==null) || 
             (this.tour!=null &&
              this.tour.equals(other.getTour()))) &&
            ((this.place==null && other.getPlace()==null) || 
             (this.place!=null &&
              this.place.equals(other.getPlace()))) &&
            ((this.order==null && other.getOrder()==null) || 
             (this.order!=null &&
              this.order.equals(other.getOrder()))) &&
            this.form == other.getForm() &&
            ((this.fms==null && other.getFms()==null) || 
             (this.fms!=null &&
              this.fms.equals(other.getFms()))) &&
            this.fmsValid == other.isFmsValid() &&
            ((this.additionalValues==null && other.getAdditionalValues()==null) || 
             (this.additionalValues!=null &&
              this.additionalValues.equals(other.getAdditionalValues()))) &&
            this.odometer == other.getOdometer() &&
            this.speed == other.getSpeed() &&
            ((this.locationInformation==null && other.getLocationInformation()==null) || 
             (this.locationInformation!=null &&
              this.locationInformation.equals(other.getLocationInformation()))) &&
            ((this.sender==null && other.getSender()==null) || 
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.formName==null && other.getFormName()==null) || 
             (this.formName!=null &&
              this.formName.equals(other.getFormName()))) &&
            ((this.driver==null && other.getDriver()==null) || 
             (this.driver!=null &&
              this.driver.equals(other.getDriver()))) &&
            ((this.coDrivers==null && other.getCoDrivers()==null) || 
             (this.coDrivers!=null &&
              java.util.Arrays.equals(this.coDrivers, other.getCoDrivers()))) &&
            ((this.attachment==null && other.getAttachment()==null) || 
             (this.attachment!=null &&
              this.attachment.equals(other.getAttachment()))) &&
            ((this.attachmentFromDispo==null && other.getAttachmentFromDispo()==null) || 
             (this.attachmentFromDispo!=null &&
              this.attachmentFromDispo.equals(other.getAttachmentFromDispo()))) &&
            ((this.gps==null && other.getGps()==null) || 
             (this.gps!=null &&
              this.gps.equals(other.getGps()))) &&
            ((this.attachedObjects==null && other.getAttachedObjects()==null) || 
             (this.attachedObjects!=null &&
              java.util.Arrays.equals(this.attachedObjects, other.getAttachedObjects()))) &&
            ((this.messageTimeUtc==null && other.getMessageTimeUtc()==null) || 
             (this.messageTimeUtc!=null &&
              this.messageTimeUtc.equals(other.getMessageTimeUtc()))) &&
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMessageID() != null) {
            _hashCode += getMessageID().hashCode();
        }
        if (getVehicle() != null) {
            _hashCode += getVehicle().hashCode();
        }
        _hashCode += new Long(getVehicleId()).hashCode();
        if (getVehicleNumberPlate() != null) {
            _hashCode += getVehicleNumberPlate().hashCode();
        }
        if (getText() != null) {
            _hashCode += getText().hashCode();
        }
        if (getReference() != null) {
            _hashCode += getReference().hashCode();
        }
        _hashCode += new Double(getLatitude()).hashCode();
        _hashCode += new Double(getLongitude()).hashCode();
        if (getTour() != null) {
            _hashCode += getTour().hashCode();
        }
        if (getPlace() != null) {
            _hashCode += getPlace().hashCode();
        }
        if (getOrder() != null) {
            _hashCode += getOrder().hashCode();
        }
        _hashCode += getForm();
        if (getFms() != null) {
            _hashCode += getFms().hashCode();
        }
        _hashCode += (isFmsValid() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getAdditionalValues() != null) {
            _hashCode += getAdditionalValues().hashCode();
        }
        _hashCode += new Double(getOdometer()).hashCode();
        _hashCode += new Double(getSpeed()).hashCode();
        if (getLocationInformation() != null) {
            _hashCode += getLocationInformation().hashCode();
        }
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getFormName() != null) {
            _hashCode += getFormName().hashCode();
        }
        if (getDriver() != null) {
            _hashCode += getDriver().hashCode();
        }
        if (getCoDrivers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCoDrivers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCoDrivers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAttachment() != null) {
            _hashCode += getAttachment().hashCode();
        }
        if (getAttachmentFromDispo() != null) {
            _hashCode += getAttachmentFromDispo().hashCode();
        }
        if (getGps() != null) {
            _hashCode += getGps().hashCode();
        }
        if (getAttachedObjects() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachedObjects());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachedObjects(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMessageTimeUtc() != null) {
            _hashCode += getMessageTimeUtc().hashCode();
        }
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Message.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MessageID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Vehicle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "VehicleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleNumberPlate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "VehicleNumberPlate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("text");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Reference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("place");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("form");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Form"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fms");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Fms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "FmsData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fmsValid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "FmsValid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalValues");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValues"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("odometer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Odometer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("speed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Speed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "LocationInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Address"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "User"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "FormName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Driver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Driver"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coDrivers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDrivers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriver"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriver"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachmentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gps");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Gps"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Gps"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachedObjects");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObjects"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObject"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObject"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageTimeUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MessageTimeUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CustomerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
