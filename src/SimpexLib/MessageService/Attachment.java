/**
 * Attachment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class Attachment  implements java.io.Serializable {
    private SimpexLib.MessageService.UrlBasedAttachment[] urlBased;

    private SimpexLib.MessageService.TextAttachment[] text;

    private SimpexLib.MessageService.BarcodeAttachment[] barcode;

    public Attachment() {
    }

    public Attachment(
           SimpexLib.MessageService.UrlBasedAttachment[] urlBased,
           SimpexLib.MessageService.TextAttachment[] text,
           SimpexLib.MessageService.BarcodeAttachment[] barcode) {
           this.urlBased = urlBased;
           this.text = text;
           this.barcode = barcode;
    }


    /**
     * Gets the urlBased value for this Attachment.
     * 
     * @return urlBased
     */
    public SimpexLib.MessageService.UrlBasedAttachment[] getUrlBased() {
        return urlBased;
    }


    /**
     * Sets the urlBased value for this Attachment.
     * 
     * @param urlBased
     */
    public void setUrlBased(SimpexLib.MessageService.UrlBasedAttachment[] urlBased) {
        this.urlBased = urlBased;
    }


    /**
     * Gets the text value for this Attachment.
     * 
     * @return text
     */
    public SimpexLib.MessageService.TextAttachment[] getText() {
        return text;
    }


    /**
     * Sets the text value for this Attachment.
     * 
     * @param text
     */
    public void setText(SimpexLib.MessageService.TextAttachment[] text) {
        this.text = text;
    }


    /**
     * Gets the barcode value for this Attachment.
     * 
     * @return barcode
     */
    public SimpexLib.MessageService.BarcodeAttachment[] getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this Attachment.
     * 
     * @param barcode
     */
    public void setBarcode(SimpexLib.MessageService.BarcodeAttachment[] barcode) {
        this.barcode = barcode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Attachment)) return false;
        Attachment other = (Attachment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.urlBased==null && other.getUrlBased()==null) || 
             (this.urlBased!=null &&
              java.util.Arrays.equals(this.urlBased, other.getUrlBased()))) &&
            ((this.text==null && other.getText()==null) || 
             (this.text!=null &&
              java.util.Arrays.equals(this.text, other.getText()))) &&
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              java.util.Arrays.equals(this.barcode, other.getBarcode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUrlBased() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUrlBased());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUrlBased(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getText() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getText());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getText(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBarcode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBarcode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBarcode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Attachment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urlBased");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UrlBased"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UrlBasedAttachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UrlBasedAttachment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("text");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TextAttachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TextAttachment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "BarcodeAttachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "BarcodeAttachment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
