/**
 * Place.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class Place  implements java.io.Serializable {
    private java.lang.String placeNr;

    private java.lang.String placeName;

    private java.lang.String address;

    private java.lang.String housenr;

    private java.lang.String nation;

    private java.lang.String zip;

    private java.lang.String location;

    private SimpexLib.MessageService.Order[] orders;

    private java.lang.String commentFromDispo;

    private java.lang.String commentToDispo;

    private java.lang.Double latitude;

    private java.lang.Double longitude;

    private java.lang.Integer displayAtPos;

    private SimpexLib.MessageService.AdditionalValues additionalData;

    private SimpexLib.MessageService.Attachment attachment;

    private SimpexLib.MessageService.Attachment attachmentFromDispo;

    private short state;

    private java.lang.String workflow;

    private java.util.Calendar planBeginUtc;

    private java.util.Calendar planEndUtc;

    private java.lang.String externalRefNr;

    public Place() {
    }

    public Place(
           java.lang.String placeNr,
           java.lang.String placeName,
           java.lang.String address,
           java.lang.String housenr,
           java.lang.String nation,
           java.lang.String zip,
           java.lang.String location,
           SimpexLib.MessageService.Order[] orders,
           java.lang.String commentFromDispo,
           java.lang.String commentToDispo,
           java.lang.Double latitude,
           java.lang.Double longitude,
           java.lang.Integer displayAtPos,
           SimpexLib.MessageService.AdditionalValues additionalData,
           SimpexLib.MessageService.Attachment attachment,
           SimpexLib.MessageService.Attachment attachmentFromDispo,
           short state,
           java.lang.String workflow,
           java.util.Calendar planBeginUtc,
           java.util.Calendar planEndUtc,
           java.lang.String externalRefNr) {
           this.placeNr = placeNr;
           this.placeName = placeName;
           this.address = address;
           this.housenr = housenr;
           this.nation = nation;
           this.zip = zip;
           this.location = location;
           this.orders = orders;
           this.commentFromDispo = commentFromDispo;
           this.commentToDispo = commentToDispo;
           this.latitude = latitude;
           this.longitude = longitude;
           this.displayAtPos = displayAtPos;
           this.additionalData = additionalData;
           this.attachment = attachment;
           this.attachmentFromDispo = attachmentFromDispo;
           this.state = state;
           this.workflow = workflow;
           this.planBeginUtc = planBeginUtc;
           this.planEndUtc = planEndUtc;
           this.externalRefNr = externalRefNr;
    }


    /**
     * Gets the placeNr value for this Place.
     * 
     * @return placeNr
     */
    public java.lang.String getPlaceNr() {
        return placeNr;
    }


    /**
     * Sets the placeNr value for this Place.
     * 
     * @param placeNr
     */
    public void setPlaceNr(java.lang.String placeNr) {
        this.placeNr = placeNr;
    }


    /**
     * Gets the placeName value for this Place.
     * 
     * @return placeName
     */
    public java.lang.String getPlaceName() {
        return placeName;
    }


    /**
     * Sets the placeName value for this Place.
     * 
     * @param placeName
     */
    public void setPlaceName(java.lang.String placeName) {
        this.placeName = placeName;
    }


    /**
     * Gets the address value for this Place.
     * 
     * @return address
     */
    public java.lang.String getAddress() {
        return address;
    }


    /**
     * Sets the address value for this Place.
     * 
     * @param address
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }


    /**
     * Gets the housenr value for this Place.
     * 
     * @return housenr
     */
    public java.lang.String getHousenr() {
        return housenr;
    }


    /**
     * Sets the housenr value for this Place.
     * 
     * @param housenr
     */
    public void setHousenr(java.lang.String housenr) {
        this.housenr = housenr;
    }


    /**
     * Gets the nation value for this Place.
     * 
     * @return nation
     */
    public java.lang.String getNation() {
        return nation;
    }


    /**
     * Sets the nation value for this Place.
     * 
     * @param nation
     */
    public void setNation(java.lang.String nation) {
        this.nation = nation;
    }


    /**
     * Gets the zip value for this Place.
     * 
     * @return zip
     */
    public java.lang.String getZip() {
        return zip;
    }


    /**
     * Sets the zip value for this Place.
     * 
     * @param zip
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }


    /**
     * Gets the location value for this Place.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this Place.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }


    /**
     * Gets the orders value for this Place.
     * 
     * @return orders
     */
    public SimpexLib.MessageService.Order[] getOrders() {
        return orders;
    }


    /**
     * Sets the orders value for this Place.
     * 
     * @param orders
     */
    public void setOrders(SimpexLib.MessageService.Order[] orders) {
        this.orders = orders;
    }


    /**
     * Gets the commentFromDispo value for this Place.
     * 
     * @return commentFromDispo
     */
    public java.lang.String getCommentFromDispo() {
        return commentFromDispo;
    }


    /**
     * Sets the commentFromDispo value for this Place.
     * 
     * @param commentFromDispo
     */
    public void setCommentFromDispo(java.lang.String commentFromDispo) {
        this.commentFromDispo = commentFromDispo;
    }


    /**
     * Gets the commentToDispo value for this Place.
     * 
     * @return commentToDispo
     */
    public java.lang.String getCommentToDispo() {
        return commentToDispo;
    }


    /**
     * Sets the commentToDispo value for this Place.
     * 
     * @param commentToDispo
     */
    public void setCommentToDispo(java.lang.String commentToDispo) {
        this.commentToDispo = commentToDispo;
    }


    /**
     * Gets the latitude value for this Place.
     * 
     * @return latitude
     */
    public java.lang.Double getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this Place.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.Double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this Place.
     * 
     * @return longitude
     */
    public java.lang.Double getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this Place.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.Double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the displayAtPos value for this Place.
     * 
     * @return displayAtPos
     */
    public java.lang.Integer getDisplayAtPos() {
        return displayAtPos;
    }


    /**
     * Sets the displayAtPos value for this Place.
     * 
     * @param displayAtPos
     */
    public void setDisplayAtPos(java.lang.Integer displayAtPos) {
        this.displayAtPos = displayAtPos;
    }


    /**
     * Gets the additionalData value for this Place.
     * 
     * @return additionalData
     */
    public SimpexLib.MessageService.AdditionalValues getAdditionalData() {
        return additionalData;
    }


    /**
     * Sets the additionalData value for this Place.
     * 
     * @param additionalData
     */
    public void setAdditionalData(SimpexLib.MessageService.AdditionalValues additionalData) {
        this.additionalData = additionalData;
    }


    /**
     * Gets the attachment value for this Place.
     * 
     * @return attachment
     */
    public SimpexLib.MessageService.Attachment getAttachment() {
        return attachment;
    }


    /**
     * Sets the attachment value for this Place.
     * 
     * @param attachment
     */
    public void setAttachment(SimpexLib.MessageService.Attachment attachment) {
        this.attachment = attachment;
    }


    /**
     * Gets the attachmentFromDispo value for this Place.
     * 
     * @return attachmentFromDispo
     */
    public SimpexLib.MessageService.Attachment getAttachmentFromDispo() {
        return attachmentFromDispo;
    }


    /**
     * Sets the attachmentFromDispo value for this Place.
     * 
     * @param attachmentFromDispo
     */
    public void setAttachmentFromDispo(SimpexLib.MessageService.Attachment attachmentFromDispo) {
        this.attachmentFromDispo = attachmentFromDispo;
    }


    /**
     * Gets the state value for this Place.
     * 
     * @return state
     */
    public short getState() {
        return state;
    }


    /**
     * Sets the state value for this Place.
     * 
     * @param state
     */
    public void setState(short state) {
        this.state = state;
    }


    /**
     * Gets the workflow value for this Place.
     * 
     * @return workflow
     */
    public java.lang.String getWorkflow() {
        return workflow;
    }


    /**
     * Sets the workflow value for this Place.
     * 
     * @param workflow
     */
    public void setWorkflow(java.lang.String workflow) {
        this.workflow = workflow;
    }


    /**
     * Gets the planBeginUtc value for this Place.
     * 
     * @return planBeginUtc
     */
    public java.util.Calendar getPlanBeginUtc() {
        return planBeginUtc;
    }


    /**
     * Sets the planBeginUtc value for this Place.
     * 
     * @param planBeginUtc
     */
    public void setPlanBeginUtc(java.util.Calendar planBeginUtc) {
        this.planBeginUtc = planBeginUtc;
    }


    /**
     * Gets the planEndUtc value for this Place.
     * 
     * @return planEndUtc
     */
    public java.util.Calendar getPlanEndUtc() {
        return planEndUtc;
    }


    /**
     * Sets the planEndUtc value for this Place.
     * 
     * @param planEndUtc
     */
    public void setPlanEndUtc(java.util.Calendar planEndUtc) {
        this.planEndUtc = planEndUtc;
    }


    /**
     * Gets the externalRefNr value for this Place.
     * 
     * @return externalRefNr
     */
    public java.lang.String getExternalRefNr() {
        return externalRefNr;
    }


    /**
     * Sets the externalRefNr value for this Place.
     * 
     * @param externalRefNr
     */
    public void setExternalRefNr(java.lang.String externalRefNr) {
        this.externalRefNr = externalRefNr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Place)) return false;
        Place other = (Place) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.placeNr==null && other.getPlaceNr()==null) || 
             (this.placeNr!=null &&
              this.placeNr.equals(other.getPlaceNr()))) &&
            ((this.placeName==null && other.getPlaceName()==null) || 
             (this.placeName!=null &&
              this.placeName.equals(other.getPlaceName()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.housenr==null && other.getHousenr()==null) || 
             (this.housenr!=null &&
              this.housenr.equals(other.getHousenr()))) &&
            ((this.nation==null && other.getNation()==null) || 
             (this.nation!=null &&
              this.nation.equals(other.getNation()))) &&
            ((this.zip==null && other.getZip()==null) || 
             (this.zip!=null &&
              this.zip.equals(other.getZip()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.orders==null && other.getOrders()==null) || 
             (this.orders!=null &&
              java.util.Arrays.equals(this.orders, other.getOrders()))) &&
            ((this.commentFromDispo==null && other.getCommentFromDispo()==null) || 
             (this.commentFromDispo!=null &&
              this.commentFromDispo.equals(other.getCommentFromDispo()))) &&
            ((this.commentToDispo==null && other.getCommentToDispo()==null) || 
             (this.commentToDispo!=null &&
              this.commentToDispo.equals(other.getCommentToDispo()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.displayAtPos==null && other.getDisplayAtPos()==null) || 
             (this.displayAtPos!=null &&
              this.displayAtPos.equals(other.getDisplayAtPos()))) &&
            ((this.additionalData==null && other.getAdditionalData()==null) || 
             (this.additionalData!=null &&
              this.additionalData.equals(other.getAdditionalData()))) &&
            ((this.attachment==null && other.getAttachment()==null) || 
             (this.attachment!=null &&
              this.attachment.equals(other.getAttachment()))) &&
            ((this.attachmentFromDispo==null && other.getAttachmentFromDispo()==null) || 
             (this.attachmentFromDispo!=null &&
              this.attachmentFromDispo.equals(other.getAttachmentFromDispo()))) &&
            this.state == other.getState() &&
            ((this.workflow==null && other.getWorkflow()==null) || 
             (this.workflow!=null &&
              this.workflow.equals(other.getWorkflow()))) &&
            ((this.planBeginUtc==null && other.getPlanBeginUtc()==null) || 
             (this.planBeginUtc!=null &&
              this.planBeginUtc.equals(other.getPlanBeginUtc()))) &&
            ((this.planEndUtc==null && other.getPlanEndUtc()==null) || 
             (this.planEndUtc!=null &&
              this.planEndUtc.equals(other.getPlanEndUtc()))) &&
            ((this.externalRefNr==null && other.getExternalRefNr()==null) || 
             (this.externalRefNr!=null &&
              this.externalRefNr.equals(other.getExternalRefNr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPlaceNr() != null) {
            _hashCode += getPlaceNr().hashCode();
        }
        if (getPlaceName() != null) {
            _hashCode += getPlaceName().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getHousenr() != null) {
            _hashCode += getHousenr().hashCode();
        }
        if (getNation() != null) {
            _hashCode += getNation().hashCode();
        }
        if (getZip() != null) {
            _hashCode += getZip().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getOrders() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrders());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrders(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCommentFromDispo() != null) {
            _hashCode += getCommentFromDispo().hashCode();
        }
        if (getCommentToDispo() != null) {
            _hashCode += getCommentToDispo().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getDisplayAtPos() != null) {
            _hashCode += getDisplayAtPos().hashCode();
        }
        if (getAdditionalData() != null) {
            _hashCode += getAdditionalData().hashCode();
        }
        if (getAttachment() != null) {
            _hashCode += getAttachment().hashCode();
        }
        if (getAttachmentFromDispo() != null) {
            _hashCode += getAttachmentFromDispo().hashCode();
        }
        _hashCode += getState();
        if (getWorkflow() != null) {
            _hashCode += getWorkflow().hashCode();
        }
        if (getPlanBeginUtc() != null) {
            _hashCode += getPlanBeginUtc().hashCode();
        }
        if (getPlanEndUtc() != null) {
            _hashCode += getPlanEndUtc().hashCode();
        }
        if (getExternalRefNr() != null) {
            _hashCode += getExternalRefNr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Place.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placeNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PlaceNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PlaceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("housenr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Housenr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Nation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Zip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orders");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Orders"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CommentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentToDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CommentToDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayAtPos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DisplayAtPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValues"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachmentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Workflow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planBeginUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PlanBeginUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planEndUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PlanEndUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalRefNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ExternalRefNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
