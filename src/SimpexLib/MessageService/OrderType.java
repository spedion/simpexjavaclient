/**
 * OrderType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class OrderType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected OrderType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Load = "Load";
    public static final java.lang.String _Unload = "Unload";
    public static final java.lang.String _AttachTrailerOrBody = "AttachTrailerOrBody";
    public static final java.lang.String _DetachTrailerOrBody = "DetachTrailerOrBody";
    public static final java.lang.String _PackageLoad = "PackageLoad";
    public static final java.lang.String _PackageUnload = "PackageUnload";
    public static final java.lang.String _EnterTrain = "EnterTrain";
    public static final java.lang.String _LeaveTrain = "LeaveTrain";
    public static final java.lang.String _EnterFerry = "EnterFerry";
    public static final java.lang.String _LeaveFerry = "LeaveFerry";
    public static final java.lang.String _PerformAction = "PerformAction";
    public static final OrderType Load = new OrderType(_Load);
    public static final OrderType Unload = new OrderType(_Unload);
    public static final OrderType AttachTrailerOrBody = new OrderType(_AttachTrailerOrBody);
    public static final OrderType DetachTrailerOrBody = new OrderType(_DetachTrailerOrBody);
    public static final OrderType PackageLoad = new OrderType(_PackageLoad);
    public static final OrderType PackageUnload = new OrderType(_PackageUnload);
    public static final OrderType EnterTrain = new OrderType(_EnterTrain);
    public static final OrderType LeaveTrain = new OrderType(_LeaveTrain);
    public static final OrderType EnterFerry = new OrderType(_EnterFerry);
    public static final OrderType LeaveFerry = new OrderType(_LeaveFerry);
    public static final OrderType PerformAction = new OrderType(_PerformAction);
    public java.lang.String getValue() { return _value_;}
    public static OrderType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        OrderType enumeration = (OrderType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static OrderType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "OrderType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
