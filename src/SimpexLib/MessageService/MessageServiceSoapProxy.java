package SimpexLib.MessageService;

public class MessageServiceSoapProxy implements SimpexLib.MessageService.MessageServiceSoap {
  private String _endpoint = null;
  private SimpexLib.MessageService.MessageServiceSoap messageServiceSoap = null;
  
  public MessageServiceSoapProxy() {
    _initMessageServiceSoapProxy();
  }
  
  public MessageServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initMessageServiceSoapProxy();
  }
  
  private void _initMessageServiceSoapProxy() {
    try {
      messageServiceSoap = (new SimpexLib.MessageService.MessageServiceLocator()).getMessageServiceSoap();
      if (messageServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)messageServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)messageServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (messageServiceSoap != null)
      ((javax.xml.rpc.Stub)messageServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap() {
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap;
  }
  
  public int getMessageCount() throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getMessageCount();
  }
  
  public boolean parseIdentifierString(java.lang.String vehicleName) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.parseIdentifierString(vehicleName);
  }
  
  public boolean parseIdentifierStringSubCustomer(java.lang.String vehicleName, int subCustomerId) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.parseIdentifierStringSubCustomer(vehicleName, subCustomerId);
  }
  
  public void getUnreadMessage(SimpexLib.MessageService.holders.ArrayOfMessageHolder getUnreadMessageResult, SimpexLib.MessageService.holders.ResultHolder r) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    messageServiceSoap.getUnreadMessage(getUnreadMessageResult, r);
  }
  
  public SimpexLib.MessageService.Message[] getNewMessage() throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getNewMessage();
  }
  
  public SimpexLib.MessageService.Message getMessageId(java.math.BigDecimal messageId) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getMessageId(messageId);
  }
  
  public SimpexLib.MessageService.Result addMessage(SimpexLib.MessageService.Message message) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.addMessage(message);
  }
  
  public SimpexLib.MessageService.PresignedUploadUrlResult getPresignedUploadUrl(java.lang.String filename, long filesize) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getPresignedUploadUrl(filename, filesize);
  }
  
  public SimpexLib.MessageService.Tour[] getToursByVehicle(java.lang.String vehicleName, int skipCount, int takeCount) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursByVehicle(vehicleName, skipCount, takeCount);
  }
  
  public SimpexLib.MessageService.Tour[] getToursByVehicleAndState(java.lang.String vehicleName, int skipCount, int takeCount, int minState, int maxState) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursByVehicleAndState(vehicleName, skipCount, takeCount, minState, maxState);
  }
  
  public SimpexLib.MessageService.Tour[] getToursBySubCustomerVehicle(int subCustomerId, java.lang.String vehicleName, int skipCount, int takeCount) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursBySubCustomerVehicle(subCustomerId, vehicleName, skipCount, takeCount);
  }
  
  public SimpexLib.MessageService.Result addRegistrationMessage(SimpexLib.MessageService.RegistrationMessage m) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.addRegistrationMessage(m);
  }
  
  public SimpexLib.MessageService.Tour[] getToursByExample(SimpexLib.MessageService.Tour example) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursByExample(example);
  }
  
  public SimpexLib.MessageService.Tour getTourByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getTourByTourNr(tourNr);
  }
  
  public SimpexLib.MessageService.CustomerTour getCustomerTourForAllSubCustomersByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getCustomerTourForAllSubCustomersByTourNr(tourNr);
  }
  
  public SimpexLib.MessageService.Tour getTourByTourNrOfCustomer(java.lang.String tourNr, int customer) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getTourByTourNrOfCustomer(tourNr, customer);
  }
  
  public SimpexLib.MessageService.Tour[] getToursByPlaceNrs(java.lang.String[] placeNrs) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursByPlaceNrs(placeNrs);
  }
  
  public SimpexLib.MessageService.Tour[] getToursByOrderNrs(java.lang.String[] orderNrs) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursByOrderNrs(orderNrs);
  }
  
  public SimpexLib.MessageService.Tour[] getToursByOrderNrExtern(java.lang.String orderNrExtern) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getToursByOrderNrExtern(orderNrExtern);
  }
  
  public SimpexLib.MessageService.Result deleteTour(java.lang.String tourNr) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.deleteTour(tourNr);
  }
  
  public SimpexLib.MessageService.Result deleteSubCustomerTour(java.lang.String tourNr, int subCustomerId) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.deleteSubCustomerTour(tourNr, subCustomerId);
  }
  
  public SimpexLib.MessageService.Result abandonTourByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.abandonTourByTourNr(tourNr);
  }
  
  public SimpexLib.MessageService.Result deleteTourObject(SimpexLib.MessageService.Tour wsTour) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.deleteTourObject(wsTour);
  }
  
  public SimpexLib.MessageService.Result updateTour(SimpexLib.MessageService.Tour newTourdata) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.updateTour(newTourdata);
  }
  
  public SimpexLib.MessageService.Result updateSubCustomerTour(SimpexLib.MessageService.Tour newTourdata, int subCustomerId) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.updateSubCustomerTour(newTourdata, subCustomerId);
  }
  
  public void reportError(java.lang.String errorReport) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    messageServiceSoap.reportError(errorReport);
  }
  
  public SimpexLib.MessageService.Result setListOfMessageAsRead(java.math.BigDecimal[] messageIdList) throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.setListOfMessageAsRead(messageIdList);
  }
  
  public java.lang.String getVersion() throws java.rmi.RemoteException{
    if (messageServiceSoap == null)
      _initMessageServiceSoapProxy();
    return messageServiceSoap.getVersion();
  }
  
  
}