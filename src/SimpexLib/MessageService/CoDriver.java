/**
 * CoDriver.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class CoDriver  extends SimpexLib.MessageService.Driver  implements java.io.Serializable {
    private SimpexLib.MessageService.CoDriverChangeEventEnum coDriverChangeEvent;

    public CoDriver() {
    }

    public CoDriver(
           java.lang.String driverCardId,
           java.lang.String language,
           java.lang.String lastName,
           java.lang.String firstName,
           java.lang.String driverPin,
           int customerId,
           SimpexLib.MessageService.CoDriverChangeEventEnum coDriverChangeEvent) {
        super(
            driverCardId,
            language,
            lastName,
            firstName,
            driverPin,
            customerId);
        this.coDriverChangeEvent = coDriverChangeEvent;
    }


    /**
     * Gets the coDriverChangeEvent value for this CoDriver.
     * 
     * @return coDriverChangeEvent
     */
    public SimpexLib.MessageService.CoDriverChangeEventEnum getCoDriverChangeEvent() {
        return coDriverChangeEvent;
    }


    /**
     * Sets the coDriverChangeEvent value for this CoDriver.
     * 
     * @param coDriverChangeEvent
     */
    public void setCoDriverChangeEvent(SimpexLib.MessageService.CoDriverChangeEventEnum coDriverChangeEvent) {
        this.coDriverChangeEvent = coDriverChangeEvent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CoDriver)) return false;
        CoDriver other = (CoDriver) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.coDriverChangeEvent==null && other.getCoDriverChangeEvent()==null) || 
             (this.coDriverChangeEvent!=null &&
              this.coDriverChangeEvent.equals(other.getCoDriverChangeEvent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCoDriverChangeEvent() != null) {
            _hashCode += getCoDriverChangeEvent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CoDriver.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriver"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coDriverChangeEvent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriverChangeEvent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriverChangeEventEnum"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
