/**
 * ResultCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class ResultCode implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ResultCode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _OtherError = "OtherError";
    public static final java.lang.String _TemporalFailure = "TemporalFailure";
    public static final java.lang.String _OkCode = "OkCode";
    public static final java.lang.String _UnknownVehicle = "UnknownVehicle";
    public static final java.lang.String _TourAlreadyPresent = "TourAlreadyPresent";
    public static final java.lang.String _ErrorCode = "ErrorCode";
    public static final ResultCode OtherError = new ResultCode(_OtherError);
    public static final ResultCode TemporalFailure = new ResultCode(_TemporalFailure);
    public static final ResultCode OkCode = new ResultCode(_OkCode);
    public static final ResultCode UnknownVehicle = new ResultCode(_UnknownVehicle);
    public static final ResultCode TourAlreadyPresent = new ResultCode(_TourAlreadyPresent);
    public static final ResultCode ErrorCode = new ResultCode(_ErrorCode);
    public java.lang.String getValue() { return _value_;}
    public static ResultCode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ResultCode enumeration = (ResultCode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ResultCode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResultCode.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ResultCode"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
