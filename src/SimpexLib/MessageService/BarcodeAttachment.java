/**
 * BarcodeAttachment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class BarcodeAttachment  implements java.io.Serializable {
    private SimpexLib.MessageService.MetaData metaData;

    private java.lang.String[] barcode;

    private java.lang.String name;  // attribute

    public BarcodeAttachment() {
    }

    public BarcodeAttachment(
           SimpexLib.MessageService.MetaData metaData,
           java.lang.String[] barcode,
           java.lang.String name) {
           this.metaData = metaData;
           this.barcode = barcode;
           this.name = name;
    }


    /**
     * Gets the metaData value for this BarcodeAttachment.
     * 
     * @return metaData
     */
    public SimpexLib.MessageService.MetaData getMetaData() {
        return metaData;
    }


    /**
     * Sets the metaData value for this BarcodeAttachment.
     * 
     * @param metaData
     */
    public void setMetaData(SimpexLib.MessageService.MetaData metaData) {
        this.metaData = metaData;
    }


    /**
     * Gets the barcode value for this BarcodeAttachment.
     * 
     * @return barcode
     */
    public java.lang.String[] getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this BarcodeAttachment.
     * 
     * @param barcode
     */
    public void setBarcode(java.lang.String[] barcode) {
        this.barcode = barcode;
    }


    /**
     * Gets the name value for this BarcodeAttachment.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this BarcodeAttachment.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BarcodeAttachment)) return false;
        BarcodeAttachment other = (BarcodeAttachment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.metaData==null && other.getMetaData()==null) || 
             (this.metaData!=null &&
              this.metaData.equals(other.getMetaData()))) &&
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              java.util.Arrays.equals(this.barcode, other.getBarcode()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMetaData() != null) {
            _hashCode += getMetaData().hashCode();
        }
        if (getBarcode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBarcode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBarcode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BarcodeAttachment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "BarcodeAttachment"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("name");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Name"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metaData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MetaData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MetaData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
