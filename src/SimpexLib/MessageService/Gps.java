/**
 * Gps.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class Gps  implements java.io.Serializable {
    private java.lang.Double accuracyInM;

    private java.lang.Double altitudeInM;

    private java.lang.Boolean fix;

    private java.lang.Double direction;

    private java.util.Calendar timestampUtc;

    public Gps() {
    }

    public Gps(
           java.lang.Double accuracyInM,
           java.lang.Double altitudeInM,
           java.lang.Boolean fix,
           java.lang.Double direction,
           java.util.Calendar timestampUtc) {
           this.accuracyInM = accuracyInM;
           this.altitudeInM = altitudeInM;
           this.fix = fix;
           this.direction = direction;
           this.timestampUtc = timestampUtc;
    }


    /**
     * Gets the accuracyInM value for this Gps.
     * 
     * @return accuracyInM
     */
    public java.lang.Double getAccuracyInM() {
        return accuracyInM;
    }


    /**
     * Sets the accuracyInM value for this Gps.
     * 
     * @param accuracyInM
     */
    public void setAccuracyInM(java.lang.Double accuracyInM) {
        this.accuracyInM = accuracyInM;
    }


    /**
     * Gets the altitudeInM value for this Gps.
     * 
     * @return altitudeInM
     */
    public java.lang.Double getAltitudeInM() {
        return altitudeInM;
    }


    /**
     * Sets the altitudeInM value for this Gps.
     * 
     * @param altitudeInM
     */
    public void setAltitudeInM(java.lang.Double altitudeInM) {
        this.altitudeInM = altitudeInM;
    }


    /**
     * Gets the fix value for this Gps.
     * 
     * @return fix
     */
    public java.lang.Boolean getFix() {
        return fix;
    }


    /**
     * Sets the fix value for this Gps.
     * 
     * @param fix
     */
    public void setFix(java.lang.Boolean fix) {
        this.fix = fix;
    }


    /**
     * Gets the direction value for this Gps.
     * 
     * @return direction
     */
    public java.lang.Double getDirection() {
        return direction;
    }


    /**
     * Sets the direction value for this Gps.
     * 
     * @param direction
     */
    public void setDirection(java.lang.Double direction) {
        this.direction = direction;
    }


    /**
     * Gets the timestampUtc value for this Gps.
     * 
     * @return timestampUtc
     */
    public java.util.Calendar getTimestampUtc() {
        return timestampUtc;
    }


    /**
     * Sets the timestampUtc value for this Gps.
     * 
     * @param timestampUtc
     */
    public void setTimestampUtc(java.util.Calendar timestampUtc) {
        this.timestampUtc = timestampUtc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Gps)) return false;
        Gps other = (Gps) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accuracyInM==null && other.getAccuracyInM()==null) || 
             (this.accuracyInM!=null &&
              this.accuracyInM.equals(other.getAccuracyInM()))) &&
            ((this.altitudeInM==null && other.getAltitudeInM()==null) || 
             (this.altitudeInM!=null &&
              this.altitudeInM.equals(other.getAltitudeInM()))) &&
            ((this.fix==null && other.getFix()==null) || 
             (this.fix!=null &&
              this.fix.equals(other.getFix()))) &&
            ((this.direction==null && other.getDirection()==null) || 
             (this.direction!=null &&
              this.direction.equals(other.getDirection()))) &&
            ((this.timestampUtc==null && other.getTimestampUtc()==null) || 
             (this.timestampUtc!=null &&
              this.timestampUtc.equals(other.getTimestampUtc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccuracyInM() != null) {
            _hashCode += getAccuracyInM().hashCode();
        }
        if (getAltitudeInM() != null) {
            _hashCode += getAltitudeInM().hashCode();
        }
        if (getFix() != null) {
            _hashCode += getFix().hashCode();
        }
        if (getDirection() != null) {
            _hashCode += getDirection().hashCode();
        }
        if (getTimestampUtc() != null) {
            _hashCode += getTimestampUtc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Gps.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Gps"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accuracyInM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AccuracyInM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altitudeInM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AltitudeInM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Fix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Direction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TimestampUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
