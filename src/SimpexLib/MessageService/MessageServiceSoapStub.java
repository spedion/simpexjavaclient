/**
 * MessageServiceSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class MessageServiceSoapStub extends org.apache.axis.client.Stub implements SimpexLib.MessageService.MessageServiceSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[28];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMessageCount");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(int.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetMessageCountResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ParseIdentifierString");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "vehicleName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        oper.setReturnClass(boolean.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ParseIdentifierStringResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ParseIdentifierStringSubCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "vehicleName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "subCustomerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        oper.setReturnClass(boolean.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ParseIdentifierStringSubCustomerResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUnreadMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetUnreadMessageResult"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfMessage"), SimpexLib.MessageService.Message[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message"));
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "r"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"), SimpexLib.MessageService.Result.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetNewMessage");
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfMessage"));
        oper.setReturnClass(SimpexLib.MessageService.Message[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetNewMessageResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMessageId");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "messageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message"));
        oper.setReturnClass(SimpexLib.MessageService.Message.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetMessageIdResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "message"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message"), SimpexLib.MessageService.Message.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AddMessageResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPresignedUploadUrl");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "filename"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "filesize"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PresignedUploadUrlResult"));
        oper.setReturnClass(SimpexLib.MessageService.PresignedUploadUrlResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetPresignedUploadUrlResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursByVehicle");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "vehicleName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "skipCount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "takeCount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByVehicleResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursByVehicleAndState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "vehicleName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "skipCount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "takeCount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "minState"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "maxState"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByVehicleAndStateResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursBySubCustomerVehicle");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "subCustomerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "vehicleName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "skipCount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "takeCount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursBySubCustomerVehicleResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddRegistrationMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "m"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RegistrationMessage"), SimpexLib.MessageService.RegistrationMessage.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AddRegistrationMessageResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursByExample");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "example"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"), SimpexLib.MessageService.Tour.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByExampleResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTourByTourNr");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourNr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetTourByTourNrResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCustomerTourForAllSubCustomersByTourNr");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourNr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CustomerTour"));
        oper.setReturnClass(SimpexLib.MessageService.CustomerTour.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetCustomerTourForAllSubCustomersByTourNrResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTourByTourNrOfCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourNr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "customer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetTourByTourNrOfCustomerResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursByPlaceNrs");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "placeNrs"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfString"), java.lang.String[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "string"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByPlaceNrsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursByOrderNrs");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "orderNrs"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfString"), java.lang.String[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "string"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByOrderNrsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetToursByOrderNrExtern");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "orderNrExtern"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour"));
        oper.setReturnClass(SimpexLib.MessageService.Tour[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByOrderNrExternResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteTour");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourNr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeleteTourResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteSubCustomerTour");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourNr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "subCustomerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeleteSubCustomerTourResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AbandonTourByTourNr");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourNr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AbandonTourByTourNrResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteTourObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "wsTour"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"), SimpexLib.MessageService.Tour.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeleteTourObjectResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateTour");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "newTourdata"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"), SimpexLib.MessageService.Tour.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UpdateTourResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateSubCustomerTour");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "newTourdata"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"), SimpexLib.MessageService.Tour.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "subCustomerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UpdateSubCustomerTourResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ReportError");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "errorReport"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SetListOfMessageAsRead");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "messageIdList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfDecimal"), java.math.BigDecimal[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "decimal"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result"));
        oper.setReturnClass(SimpexLib.MessageService.Result.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "SetListOfMessageAsReadResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetVersion");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetVersionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

    }

    public MessageServiceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public MessageServiceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public MessageServiceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValueItem");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.AdditionalValueItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValues");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.AdditionalValues.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Address");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Address.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfAdditionalValueItem");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.AdditionalValueItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValueItem");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValueItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfAttachedObject");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.AttachedObject[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObject");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObject");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfBarcodeAttachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.BarcodeAttachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "BarcodeAttachment");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "BarcodeAttachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfCoDriver");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.CoDriver[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriver");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriver");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfDecimal");
            cachedSerQNames.add(qName);
            cls = java.math.BigDecimal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "decimal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfMessage");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Message[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfOrder");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Order[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfPlace");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Place[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfString");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTextAttachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.TextAttachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TextAttachment");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TextAttachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfTour");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Tour[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArrayOfUrlBasedAttachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.UrlBasedAttachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UrlBasedAttachment");
            qName2 = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UrlBasedAttachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObject");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.AttachedObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Attachment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "BarcodeAttachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.BarcodeAttachment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriver");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.CoDriver.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CoDriverChangeEventEnum");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.CoDriverChangeEventEnum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CustomerTour");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.CustomerTour.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Driver");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Driver.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ErrorCode");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.ErrorCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "FmsData");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.FmsData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Gps");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Gps.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Message");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Message.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MetaData");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.MetaData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Order.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "OrderType");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.OrderType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Place.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Position");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Position.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PresignedUploadUrlResult");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.PresignedUploadUrlResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RegistrationMessage");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.RegistrationMessage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RegistrationMessageTextType");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.RegistrationMessageTextType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Result");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Result.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ResultCode");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.ResultCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TextAttachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.TextAttachment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.Tour.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UrlBasedAttachment");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.UrlBasedAttachment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "User");
            cachedSerQNames.add(qName);
            cls = SimpexLib.MessageService.User.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public int getMessageCount() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetMessageCount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetMessageCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Integer) _resp).intValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Integer) org.apache.axis.utils.JavaUtils.convert(_resp, int.class)).intValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public boolean parseIdentifierString(java.lang.String vehicleName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/ParseIdentifierString");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ParseIdentifierString"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {vehicleName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Boolean) _resp).booleanValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, boolean.class)).booleanValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public boolean parseIdentifierStringSubCustomer(java.lang.String vehicleName, int subCustomerId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/ParseIdentifierStringSubCustomer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ParseIdentifierStringSubCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {vehicleName, new java.lang.Integer(subCustomerId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Boolean) _resp).booleanValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, boolean.class)).booleanValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getUnreadMessage(SimpexLib.MessageService.holders.ArrayOfMessageHolder getUnreadMessageResult, SimpexLib.MessageService.holders.ResultHolder r) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetUnreadMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetUnreadMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                getUnreadMessageResult.value = (SimpexLib.MessageService.Message[]) _output.get(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetUnreadMessageResult"));
            } catch (java.lang.Exception _exception) {
                getUnreadMessageResult.value = (SimpexLib.MessageService.Message[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetUnreadMessageResult")), SimpexLib.MessageService.Message[].class);
            }
            try {
                r.value = (SimpexLib.MessageService.Result) _output.get(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "r"));
            } catch (java.lang.Exception _exception) {
                r.value = (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "r")), SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Message[] getNewMessage() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetNewMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetNewMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Message[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Message[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Message[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Message getMessageId(java.math.BigDecimal messageId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetMessageId");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetMessageId"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {messageId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Message) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Message) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Message.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result addMessage(SimpexLib.MessageService.Message message) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/AddMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AddMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {message});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.PresignedUploadUrlResult getPresignedUploadUrl(java.lang.String filename, long filesize) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetPresignedUploadUrl");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetPresignedUploadUrl"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {filename, new java.lang.Long(filesize)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.PresignedUploadUrlResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.PresignedUploadUrlResult) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.PresignedUploadUrlResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursByVehicle(java.lang.String vehicleName, int skipCount, int takeCount) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursByVehicle");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByVehicle"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {vehicleName, new java.lang.Integer(skipCount), new java.lang.Integer(takeCount)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursByVehicleAndState(java.lang.String vehicleName, int skipCount, int takeCount, int minState, int maxState) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursByVehicleAndState");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByVehicleAndState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {vehicleName, new java.lang.Integer(skipCount), new java.lang.Integer(takeCount), new java.lang.Integer(minState), new java.lang.Integer(maxState)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursBySubCustomerVehicle(int subCustomerId, java.lang.String vehicleName, int skipCount, int takeCount) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursBySubCustomerVehicle");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursBySubCustomerVehicle"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(subCustomerId), vehicleName, new java.lang.Integer(skipCount), new java.lang.Integer(takeCount)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result addRegistrationMessage(SimpexLib.MessageService.RegistrationMessage m) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/AddRegistrationMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AddRegistrationMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {m});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursByExample(SimpexLib.MessageService.Tour example) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursByExample");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByExample"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {example});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour getTourByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetTourByTourNr");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetTourByTourNr"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tourNr});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.CustomerTour getCustomerTourForAllSubCustomersByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetCustomerTourForAllSubCustomersByTourNr");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetCustomerTourForAllSubCustomersByTourNr"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tourNr});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.CustomerTour) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.CustomerTour) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.CustomerTour.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour getTourByTourNrOfCustomer(java.lang.String tourNr, int customer) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetTourByTourNrOfCustomer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetTourByTourNrOfCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tourNr, new java.lang.Integer(customer)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursByPlaceNrs(java.lang.String[] placeNrs) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursByPlaceNrs");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByPlaceNrs"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {placeNrs});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursByOrderNrs(java.lang.String[] orderNrs) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursByOrderNrs");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByOrderNrs"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {orderNrs});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Tour[] getToursByOrderNrExtern(java.lang.String orderNrExtern) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetToursByOrderNrExtern");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetToursByOrderNrExtern"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {orderNrExtern});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Tour[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Tour[]) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Tour[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result deleteTour(java.lang.String tourNr) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/DeleteTour");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeleteTour"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tourNr});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result deleteSubCustomerTour(java.lang.String tourNr, int subCustomerId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/DeleteSubCustomerTour");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeleteSubCustomerTour"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tourNr, new java.lang.Integer(subCustomerId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result abandonTourByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/AbandonTourByTourNr");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AbandonTourByTourNr"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tourNr});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result deleteTourObject(SimpexLib.MessageService.Tour wsTour) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/DeleteTourObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeleteTourObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {wsTour});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result updateTour(SimpexLib.MessageService.Tour newTourdata) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/UpdateTour");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UpdateTour"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {newTourdata});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result updateSubCustomerTour(SimpexLib.MessageService.Tour newTourdata, int subCustomerId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/UpdateSubCustomerTour");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UpdateSubCustomerTour"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {newTourdata, new java.lang.Integer(subCustomerId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void reportError(java.lang.String errorReport) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/ReportError");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ReportError"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {errorReport});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public SimpexLib.MessageService.Result setListOfMessageAsRead(java.math.BigDecimal[] messageIdList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/SetListOfMessageAsRead");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "SetListOfMessageAsRead"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {messageIdList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (SimpexLib.MessageService.Result) _resp;
            } catch (java.lang.Exception _exception) {
                return (SimpexLib.MessageService.Result) org.apache.axis.utils.JavaUtils.convert(_resp, SimpexLib.MessageService.Result.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getVersion() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.spedion.de/simpex/0.50/GetVersion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "GetVersion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
