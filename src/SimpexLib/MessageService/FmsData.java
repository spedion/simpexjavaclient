/**
 * FmsData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class FmsData  implements java.io.Serializable {
    private double speed;

    private double totalfuel;

    private double odometer;

    private double fuellevel;

    private double nextServiceStop;

    private double engineWorkTime;

    private java.util.Calendar timestampUtc;

    public FmsData() {
    }

    public FmsData(
           double speed,
           double totalfuel,
           double odometer,
           double fuellevel,
           double nextServiceStop,
           double engineWorkTime,
           java.util.Calendar timestampUtc) {
           this.speed = speed;
           this.totalfuel = totalfuel;
           this.odometer = odometer;
           this.fuellevel = fuellevel;
           this.nextServiceStop = nextServiceStop;
           this.engineWorkTime = engineWorkTime;
           this.timestampUtc = timestampUtc;
    }


    /**
     * Gets the speed value for this FmsData.
     * 
     * @return speed
     */
    public double getSpeed() {
        return speed;
    }


    /**
     * Sets the speed value for this FmsData.
     * 
     * @param speed
     */
    public void setSpeed(double speed) {
        this.speed = speed;
    }


    /**
     * Gets the totalfuel value for this FmsData.
     * 
     * @return totalfuel
     */
    public double getTotalfuel() {
        return totalfuel;
    }


    /**
     * Sets the totalfuel value for this FmsData.
     * 
     * @param totalfuel
     */
    public void setTotalfuel(double totalfuel) {
        this.totalfuel = totalfuel;
    }


    /**
     * Gets the odometer value for this FmsData.
     * 
     * @return odometer
     */
    public double getOdometer() {
        return odometer;
    }


    /**
     * Sets the odometer value for this FmsData.
     * 
     * @param odometer
     */
    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }


    /**
     * Gets the fuellevel value for this FmsData.
     * 
     * @return fuellevel
     */
    public double getFuellevel() {
        return fuellevel;
    }


    /**
     * Sets the fuellevel value for this FmsData.
     * 
     * @param fuellevel
     */
    public void setFuellevel(double fuellevel) {
        this.fuellevel = fuellevel;
    }


    /**
     * Gets the nextServiceStop value for this FmsData.
     * 
     * @return nextServiceStop
     */
    public double getNextServiceStop() {
        return nextServiceStop;
    }


    /**
     * Sets the nextServiceStop value for this FmsData.
     * 
     * @param nextServiceStop
     */
    public void setNextServiceStop(double nextServiceStop) {
        this.nextServiceStop = nextServiceStop;
    }


    /**
     * Gets the engineWorkTime value for this FmsData.
     * 
     * @return engineWorkTime
     */
    public double getEngineWorkTime() {
        return engineWorkTime;
    }


    /**
     * Sets the engineWorkTime value for this FmsData.
     * 
     * @param engineWorkTime
     */
    public void setEngineWorkTime(double engineWorkTime) {
        this.engineWorkTime = engineWorkTime;
    }


    /**
     * Gets the timestampUtc value for this FmsData.
     * 
     * @return timestampUtc
     */
    public java.util.Calendar getTimestampUtc() {
        return timestampUtc;
    }


    /**
     * Sets the timestampUtc value for this FmsData.
     * 
     * @param timestampUtc
     */
    public void setTimestampUtc(java.util.Calendar timestampUtc) {
        this.timestampUtc = timestampUtc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FmsData)) return false;
        FmsData other = (FmsData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.speed == other.getSpeed() &&
            this.totalfuel == other.getTotalfuel() &&
            this.odometer == other.getOdometer() &&
            this.fuellevel == other.getFuellevel() &&
            this.nextServiceStop == other.getNextServiceStop() &&
            this.engineWorkTime == other.getEngineWorkTime() &&
            ((this.timestampUtc==null && other.getTimestampUtc()==null) || 
             (this.timestampUtc!=null &&
              this.timestampUtc.equals(other.getTimestampUtc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Double(getSpeed()).hashCode();
        _hashCode += new Double(getTotalfuel()).hashCode();
        _hashCode += new Double(getOdometer()).hashCode();
        _hashCode += new Double(getFuellevel()).hashCode();
        _hashCode += new Double(getNextServiceStop()).hashCode();
        _hashCode += new Double(getEngineWorkTime()).hashCode();
        if (getTimestampUtc() != null) {
            _hashCode += getTimestampUtc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FmsData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "FmsData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("speed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Speed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalfuel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Totalfuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("odometer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Odometer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuellevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Fuellevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nextServiceStop");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "NextServiceStop"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineWorkTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "EngineWorkTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TimestampUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
