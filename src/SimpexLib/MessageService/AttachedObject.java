/**
 * AttachedObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class AttachedObject  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String name;

    private java.lang.String type;

    private java.lang.String change;

    private java.lang.String subCategory;

    private java.lang.String attachedToId;

    private java.lang.String attachedToName;

    public AttachedObject() {
    }

    public AttachedObject(
           java.lang.String id,
           java.lang.String name,
           java.lang.String type,
           java.lang.String change,
           java.lang.String subCategory,
           java.lang.String attachedToId,
           java.lang.String attachedToName) {
           this.id = id;
           this.name = name;
           this.type = type;
           this.change = change;
           this.subCategory = subCategory;
           this.attachedToId = attachedToId;
           this.attachedToName = attachedToName;
    }


    /**
     * Gets the id value for this AttachedObject.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this AttachedObject.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the name value for this AttachedObject.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this AttachedObject.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the type value for this AttachedObject.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this AttachedObject.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the change value for this AttachedObject.
     * 
     * @return change
     */
    public java.lang.String getChange() {
        return change;
    }


    /**
     * Sets the change value for this AttachedObject.
     * 
     * @param change
     */
    public void setChange(java.lang.String change) {
        this.change = change;
    }


    /**
     * Gets the subCategory value for this AttachedObject.
     * 
     * @return subCategory
     */
    public java.lang.String getSubCategory() {
        return subCategory;
    }


    /**
     * Sets the subCategory value for this AttachedObject.
     * 
     * @param subCategory
     */
    public void setSubCategory(java.lang.String subCategory) {
        this.subCategory = subCategory;
    }


    /**
     * Gets the attachedToId value for this AttachedObject.
     * 
     * @return attachedToId
     */
    public java.lang.String getAttachedToId() {
        return attachedToId;
    }


    /**
     * Sets the attachedToId value for this AttachedObject.
     * 
     * @param attachedToId
     */
    public void setAttachedToId(java.lang.String attachedToId) {
        this.attachedToId = attachedToId;
    }


    /**
     * Gets the attachedToName value for this AttachedObject.
     * 
     * @return attachedToName
     */
    public java.lang.String getAttachedToName() {
        return attachedToName;
    }


    /**
     * Sets the attachedToName value for this AttachedObject.
     * 
     * @param attachedToName
     */
    public void setAttachedToName(java.lang.String attachedToName) {
        this.attachedToName = attachedToName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AttachedObject)) return false;
        AttachedObject other = (AttachedObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.change==null && other.getChange()==null) || 
             (this.change!=null &&
              this.change.equals(other.getChange()))) &&
            ((this.subCategory==null && other.getSubCategory()==null) || 
             (this.subCategory!=null &&
              this.subCategory.equals(other.getSubCategory()))) &&
            ((this.attachedToId==null && other.getAttachedToId()==null) || 
             (this.attachedToId!=null &&
              this.attachedToId.equals(other.getAttachedToId()))) &&
            ((this.attachedToName==null && other.getAttachedToName()==null) || 
             (this.attachedToName!=null &&
              this.attachedToName.equals(other.getAttachedToName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getChange() != null) {
            _hashCode += getChange().hashCode();
        }
        if (getSubCategory() != null) {
            _hashCode += getSubCategory().hashCode();
        }
        if (getAttachedToId() != null) {
            _hashCode += getAttachedToId().hashCode();
        }
        if (getAttachedToName() != null) {
            _hashCode += getAttachedToName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AttachedObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("change");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Change"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "SubCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachedToId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedToId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachedToName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachedToName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
