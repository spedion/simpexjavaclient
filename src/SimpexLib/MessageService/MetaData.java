/**
 * MetaData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class MetaData  implements java.io.Serializable {
    private java.lang.String workflowActionLabel;

    private java.lang.String actionInputResultAttribute;

    private java.lang.String attribute;

    private java.lang.Integer sortNo;

    private java.lang.Integer sourceEventForm;

    private SimpexLib.MessageService.Position position;

    private java.util.Calendar timestampUtc;

    public MetaData() {
    }

    public MetaData(
           java.lang.String workflowActionLabel,
           java.lang.String actionInputResultAttribute,
           java.lang.String attribute,
           java.lang.Integer sortNo,
           java.lang.Integer sourceEventForm,
           SimpexLib.MessageService.Position position,
           java.util.Calendar timestampUtc) {
           this.workflowActionLabel = workflowActionLabel;
           this.actionInputResultAttribute = actionInputResultAttribute;
           this.attribute = attribute;
           this.sortNo = sortNo;
           this.sourceEventForm = sourceEventForm;
           this.position = position;
           this.timestampUtc = timestampUtc;
    }


    /**
     * Gets the workflowActionLabel value for this MetaData.
     * 
     * @return workflowActionLabel
     */
    public java.lang.String getWorkflowActionLabel() {
        return workflowActionLabel;
    }


    /**
     * Sets the workflowActionLabel value for this MetaData.
     * 
     * @param workflowActionLabel
     */
    public void setWorkflowActionLabel(java.lang.String workflowActionLabel) {
        this.workflowActionLabel = workflowActionLabel;
    }


    /**
     * Gets the actionInputResultAttribute value for this MetaData.
     * 
     * @return actionInputResultAttribute
     */
    public java.lang.String getActionInputResultAttribute() {
        return actionInputResultAttribute;
    }


    /**
     * Sets the actionInputResultAttribute value for this MetaData.
     * 
     * @param actionInputResultAttribute
     */
    public void setActionInputResultAttribute(java.lang.String actionInputResultAttribute) {
        this.actionInputResultAttribute = actionInputResultAttribute;
    }


    /**
     * Gets the attribute value for this MetaData.
     * 
     * @return attribute
     */
    public java.lang.String getAttribute() {
        return attribute;
    }


    /**
     * Sets the attribute value for this MetaData.
     * 
     * @param attribute
     */
    public void setAttribute(java.lang.String attribute) {
        this.attribute = attribute;
    }


    /**
     * Gets the sortNo value for this MetaData.
     * 
     * @return sortNo
     */
    public java.lang.Integer getSortNo() {
        return sortNo;
    }


    /**
     * Sets the sortNo value for this MetaData.
     * 
     * @param sortNo
     */
    public void setSortNo(java.lang.Integer sortNo) {
        this.sortNo = sortNo;
    }


    /**
     * Gets the sourceEventForm value for this MetaData.
     * 
     * @return sourceEventForm
     */
    public java.lang.Integer getSourceEventForm() {
        return sourceEventForm;
    }


    /**
     * Sets the sourceEventForm value for this MetaData.
     * 
     * @param sourceEventForm
     */
    public void setSourceEventForm(java.lang.Integer sourceEventForm) {
        this.sourceEventForm = sourceEventForm;
    }


    /**
     * Gets the position value for this MetaData.
     * 
     * @return position
     */
    public SimpexLib.MessageService.Position getPosition() {
        return position;
    }


    /**
     * Sets the position value for this MetaData.
     * 
     * @param position
     */
    public void setPosition(SimpexLib.MessageService.Position position) {
        this.position = position;
    }


    /**
     * Gets the timestampUtc value for this MetaData.
     * 
     * @return timestampUtc
     */
    public java.util.Calendar getTimestampUtc() {
        return timestampUtc;
    }


    /**
     * Sets the timestampUtc value for this MetaData.
     * 
     * @param timestampUtc
     */
    public void setTimestampUtc(java.util.Calendar timestampUtc) {
        this.timestampUtc = timestampUtc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetaData)) return false;
        MetaData other = (MetaData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.workflowActionLabel==null && other.getWorkflowActionLabel()==null) || 
             (this.workflowActionLabel!=null &&
              this.workflowActionLabel.equals(other.getWorkflowActionLabel()))) &&
            ((this.actionInputResultAttribute==null && other.getActionInputResultAttribute()==null) || 
             (this.actionInputResultAttribute!=null &&
              this.actionInputResultAttribute.equals(other.getActionInputResultAttribute()))) &&
            ((this.attribute==null && other.getAttribute()==null) || 
             (this.attribute!=null &&
              this.attribute.equals(other.getAttribute()))) &&
            ((this.sortNo==null && other.getSortNo()==null) || 
             (this.sortNo!=null &&
              this.sortNo.equals(other.getSortNo()))) &&
            ((this.sourceEventForm==null && other.getSourceEventForm()==null) || 
             (this.sourceEventForm!=null &&
              this.sourceEventForm.equals(other.getSourceEventForm()))) &&
            ((this.position==null && other.getPosition()==null) || 
             (this.position!=null &&
              this.position.equals(other.getPosition()))) &&
            ((this.timestampUtc==null && other.getTimestampUtc()==null) || 
             (this.timestampUtc!=null &&
              this.timestampUtc.equals(other.getTimestampUtc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWorkflowActionLabel() != null) {
            _hashCode += getWorkflowActionLabel().hashCode();
        }
        if (getActionInputResultAttribute() != null) {
            _hashCode += getActionInputResultAttribute().hashCode();
        }
        if (getAttribute() != null) {
            _hashCode += getAttribute().hashCode();
        }
        if (getSortNo() != null) {
            _hashCode += getSortNo().hashCode();
        }
        if (getSourceEventForm() != null) {
            _hashCode += getSourceEventForm().hashCode();
        }
        if (getPosition() != null) {
            _hashCode += getPosition().hashCode();
        }
        if (getTimestampUtc() != null) {
            _hashCode += getTimestampUtc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetaData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MetaData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflowActionLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "WorkflowActionLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionInputResultAttribute");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ActionInputResultAttribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sortNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "SortNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceEventForm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "SourceEventForm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("position");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Position"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Position"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TimestampUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
