/**
 * Tour.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class Tour  implements java.io.Serializable {
    private java.lang.String tournr;

    private java.lang.String driverPin;

    private int state;

    private SimpexLib.MessageService.Place[] places;

    private SimpexLib.MessageService.AdditionalValues additionalData;

    private java.lang.String requiredTrailer;

    private int displayAtPos;

    private java.lang.String vehicleName;

    private SimpexLib.MessageService.Attachment attachment;

    private SimpexLib.MessageService.Attachment attachmentFromDispo;

    private java.lang.String workflow;

    private java.lang.String commentFromDispo;

    private java.lang.String commentToDispo;

    private java.util.Calendar tourBeginUtc;

    private java.util.Calendar tourEndUtc;

    private java.util.Calendar tourDateUtc;

    public Tour() {
    }

    public Tour(
           java.lang.String tournr,
           java.lang.String driverPin,
           int state,
           SimpexLib.MessageService.Place[] places,
           SimpexLib.MessageService.AdditionalValues additionalData,
           java.lang.String requiredTrailer,
           int displayAtPos,
           java.lang.String vehicleName,
           SimpexLib.MessageService.Attachment attachment,
           SimpexLib.MessageService.Attachment attachmentFromDispo,
           java.lang.String workflow,
           java.lang.String commentFromDispo,
           java.lang.String commentToDispo,
           java.util.Calendar tourBeginUtc,
           java.util.Calendar tourEndUtc,
           java.util.Calendar tourDateUtc) {
           this.tournr = tournr;
           this.driverPin = driverPin;
           this.state = state;
           this.places = places;
           this.additionalData = additionalData;
           this.requiredTrailer = requiredTrailer;
           this.displayAtPos = displayAtPos;
           this.vehicleName = vehicleName;
           this.attachment = attachment;
           this.attachmentFromDispo = attachmentFromDispo;
           this.workflow = workflow;
           this.commentFromDispo = commentFromDispo;
           this.commentToDispo = commentToDispo;
           this.tourBeginUtc = tourBeginUtc;
           this.tourEndUtc = tourEndUtc;
           this.tourDateUtc = tourDateUtc;
    }


    /**
     * Gets the tournr value for this Tour.
     * 
     * @return tournr
     */
    public java.lang.String getTournr() {
        return tournr;
    }


    /**
     * Sets the tournr value for this Tour.
     * 
     * @param tournr
     */
    public void setTournr(java.lang.String tournr) {
        this.tournr = tournr;
    }


    /**
     * Gets the driverPin value for this Tour.
     * 
     * @return driverPin
     */
    public java.lang.String getDriverPin() {
        return driverPin;
    }


    /**
     * Sets the driverPin value for this Tour.
     * 
     * @param driverPin
     */
    public void setDriverPin(java.lang.String driverPin) {
        this.driverPin = driverPin;
    }


    /**
     * Gets the state value for this Tour.
     * 
     * @return state
     */
    public int getState() {
        return state;
    }


    /**
     * Sets the state value for this Tour.
     * 
     * @param state
     */
    public void setState(int state) {
        this.state = state;
    }


    /**
     * Gets the places value for this Tour.
     * 
     * @return places
     */
    public SimpexLib.MessageService.Place[] getPlaces() {
        return places;
    }


    /**
     * Sets the places value for this Tour.
     * 
     * @param places
     */
    public void setPlaces(SimpexLib.MessageService.Place[] places) {
        this.places = places;
    }


    /**
     * Gets the additionalData value for this Tour.
     * 
     * @return additionalData
     */
    public SimpexLib.MessageService.AdditionalValues getAdditionalData() {
        return additionalData;
    }


    /**
     * Sets the additionalData value for this Tour.
     * 
     * @param additionalData
     */
    public void setAdditionalData(SimpexLib.MessageService.AdditionalValues additionalData) {
        this.additionalData = additionalData;
    }


    /**
     * Gets the requiredTrailer value for this Tour.
     * 
     * @return requiredTrailer
     */
    public java.lang.String getRequiredTrailer() {
        return requiredTrailer;
    }


    /**
     * Sets the requiredTrailer value for this Tour.
     * 
     * @param requiredTrailer
     */
    public void setRequiredTrailer(java.lang.String requiredTrailer) {
        this.requiredTrailer = requiredTrailer;
    }


    /**
     * Gets the displayAtPos value for this Tour.
     * 
     * @return displayAtPos
     */
    public int getDisplayAtPos() {
        return displayAtPos;
    }


    /**
     * Sets the displayAtPos value for this Tour.
     * 
     * @param displayAtPos
     */
    public void setDisplayAtPos(int displayAtPos) {
        this.displayAtPos = displayAtPos;
    }


    /**
     * Gets the vehicleName value for this Tour.
     * 
     * @return vehicleName
     */
    public java.lang.String getVehicleName() {
        return vehicleName;
    }


    /**
     * Sets the vehicleName value for this Tour.
     * 
     * @param vehicleName
     */
    public void setVehicleName(java.lang.String vehicleName) {
        this.vehicleName = vehicleName;
    }


    /**
     * Gets the attachment value for this Tour.
     * 
     * @return attachment
     */
    public SimpexLib.MessageService.Attachment getAttachment() {
        return attachment;
    }


    /**
     * Sets the attachment value for this Tour.
     * 
     * @param attachment
     */
    public void setAttachment(SimpexLib.MessageService.Attachment attachment) {
        this.attachment = attachment;
    }


    /**
     * Gets the attachmentFromDispo value for this Tour.
     * 
     * @return attachmentFromDispo
     */
    public SimpexLib.MessageService.Attachment getAttachmentFromDispo() {
        return attachmentFromDispo;
    }


    /**
     * Sets the attachmentFromDispo value for this Tour.
     * 
     * @param attachmentFromDispo
     */
    public void setAttachmentFromDispo(SimpexLib.MessageService.Attachment attachmentFromDispo) {
        this.attachmentFromDispo = attachmentFromDispo;
    }


    /**
     * Gets the workflow value for this Tour.
     * 
     * @return workflow
     */
    public java.lang.String getWorkflow() {
        return workflow;
    }


    /**
     * Sets the workflow value for this Tour.
     * 
     * @param workflow
     */
    public void setWorkflow(java.lang.String workflow) {
        this.workflow = workflow;
    }


    /**
     * Gets the commentFromDispo value for this Tour.
     * 
     * @return commentFromDispo
     */
    public java.lang.String getCommentFromDispo() {
        return commentFromDispo;
    }


    /**
     * Sets the commentFromDispo value for this Tour.
     * 
     * @param commentFromDispo
     */
    public void setCommentFromDispo(java.lang.String commentFromDispo) {
        this.commentFromDispo = commentFromDispo;
    }


    /**
     * Gets the commentToDispo value for this Tour.
     * 
     * @return commentToDispo
     */
    public java.lang.String getCommentToDispo() {
        return commentToDispo;
    }


    /**
     * Sets the commentToDispo value for this Tour.
     * 
     * @param commentToDispo
     */
    public void setCommentToDispo(java.lang.String commentToDispo) {
        this.commentToDispo = commentToDispo;
    }


    /**
     * Gets the tourBeginUtc value for this Tour.
     * 
     * @return tourBeginUtc
     */
    public java.util.Calendar getTourBeginUtc() {
        return tourBeginUtc;
    }


    /**
     * Sets the tourBeginUtc value for this Tour.
     * 
     * @param tourBeginUtc
     */
    public void setTourBeginUtc(java.util.Calendar tourBeginUtc) {
        this.tourBeginUtc = tourBeginUtc;
    }


    /**
     * Gets the tourEndUtc value for this Tour.
     * 
     * @return tourEndUtc
     */
    public java.util.Calendar getTourEndUtc() {
        return tourEndUtc;
    }


    /**
     * Sets the tourEndUtc value for this Tour.
     * 
     * @param tourEndUtc
     */
    public void setTourEndUtc(java.util.Calendar tourEndUtc) {
        this.tourEndUtc = tourEndUtc;
    }


    /**
     * Gets the tourDateUtc value for this Tour.
     * 
     * @return tourDateUtc
     */
    public java.util.Calendar getTourDateUtc() {
        return tourDateUtc;
    }


    /**
     * Sets the tourDateUtc value for this Tour.
     * 
     * @param tourDateUtc
     */
    public void setTourDateUtc(java.util.Calendar tourDateUtc) {
        this.tourDateUtc = tourDateUtc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tour)) return false;
        Tour other = (Tour) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tournr==null && other.getTournr()==null) || 
             (this.tournr!=null &&
              this.tournr.equals(other.getTournr()))) &&
            ((this.driverPin==null && other.getDriverPin()==null) || 
             (this.driverPin!=null &&
              this.driverPin.equals(other.getDriverPin()))) &&
            this.state == other.getState() &&
            ((this.places==null && other.getPlaces()==null) || 
             (this.places!=null &&
              java.util.Arrays.equals(this.places, other.getPlaces()))) &&
            ((this.additionalData==null && other.getAdditionalData()==null) || 
             (this.additionalData!=null &&
              this.additionalData.equals(other.getAdditionalData()))) &&
            ((this.requiredTrailer==null && other.getRequiredTrailer()==null) || 
             (this.requiredTrailer!=null &&
              this.requiredTrailer.equals(other.getRequiredTrailer()))) &&
            this.displayAtPos == other.getDisplayAtPos() &&
            ((this.vehicleName==null && other.getVehicleName()==null) || 
             (this.vehicleName!=null &&
              this.vehicleName.equals(other.getVehicleName()))) &&
            ((this.attachment==null && other.getAttachment()==null) || 
             (this.attachment!=null &&
              this.attachment.equals(other.getAttachment()))) &&
            ((this.attachmentFromDispo==null && other.getAttachmentFromDispo()==null) || 
             (this.attachmentFromDispo!=null &&
              this.attachmentFromDispo.equals(other.getAttachmentFromDispo()))) &&
            ((this.workflow==null && other.getWorkflow()==null) || 
             (this.workflow!=null &&
              this.workflow.equals(other.getWorkflow()))) &&
            ((this.commentFromDispo==null && other.getCommentFromDispo()==null) || 
             (this.commentFromDispo!=null &&
              this.commentFromDispo.equals(other.getCommentFromDispo()))) &&
            ((this.commentToDispo==null && other.getCommentToDispo()==null) || 
             (this.commentToDispo!=null &&
              this.commentToDispo.equals(other.getCommentToDispo()))) &&
            ((this.tourBeginUtc==null && other.getTourBeginUtc()==null) || 
             (this.tourBeginUtc!=null &&
              this.tourBeginUtc.equals(other.getTourBeginUtc()))) &&
            ((this.tourEndUtc==null && other.getTourEndUtc()==null) || 
             (this.tourEndUtc!=null &&
              this.tourEndUtc.equals(other.getTourEndUtc()))) &&
            ((this.tourDateUtc==null && other.getTourDateUtc()==null) || 
             (this.tourDateUtc!=null &&
              this.tourDateUtc.equals(other.getTourDateUtc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTournr() != null) {
            _hashCode += getTournr().hashCode();
        }
        if (getDriverPin() != null) {
            _hashCode += getDriverPin().hashCode();
        }
        _hashCode += getState();
        if (getPlaces() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPlaces());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPlaces(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAdditionalData() != null) {
            _hashCode += getAdditionalData().hashCode();
        }
        if (getRequiredTrailer() != null) {
            _hashCode += getRequiredTrailer().hashCode();
        }
        _hashCode += getDisplayAtPos();
        if (getVehicleName() != null) {
            _hashCode += getVehicleName().hashCode();
        }
        if (getAttachment() != null) {
            _hashCode += getAttachment().hashCode();
        }
        if (getAttachmentFromDispo() != null) {
            _hashCode += getAttachmentFromDispo().hashCode();
        }
        if (getWorkflow() != null) {
            _hashCode += getWorkflow().hashCode();
        }
        if (getCommentFromDispo() != null) {
            _hashCode += getCommentFromDispo().hashCode();
        }
        if (getCommentToDispo() != null) {
            _hashCode += getCommentToDispo().hashCode();
        }
        if (getTourBeginUtc() != null) {
            _hashCode += getTourBeginUtc().hashCode();
        }
        if (getTourEndUtc() != null) {
            _hashCode += getTourEndUtc().hashCode();
        }
        if (getTourDateUtc() != null) {
            _hashCode += getTourDateUtc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tour.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tour"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tournr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Tournr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driverPin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DriverPin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("places");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Places"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Place"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValues"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requiredTrailer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RequiredTrailer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayAtPos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DisplayAtPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "VehicleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachmentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Workflow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CommentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentToDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CommentToDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tourBeginUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourBeginUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tourEndUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourEndUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tourDateUtc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TourDateUtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
