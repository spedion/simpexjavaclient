/**
 * Order.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class Order  implements java.io.Serializable {
    private SimpexLib.MessageService.AdditionalValues additionalData;

    private java.lang.String commentToDispo;

    private java.lang.String orderNr;

    private SimpexLib.MessageService.OrderType type;

    private java.lang.String productName;

    private java.lang.Integer amount;

    private java.lang.Integer amount2;

    private java.lang.String packing;

    private java.lang.String packing2;

    private java.lang.Double weight;

    private java.lang.String hazard;

    private java.lang.String trailer;

    private java.lang.String container1;

    private java.lang.String container2;

    private int state;

    private java.lang.String orderNrExtern;

    private java.lang.Integer displayAtPos;

    private java.lang.String name1;

    private java.lang.String name2;

    private java.lang.String artNr;

    private java.lang.String charge;

    private java.lang.String deliveryNr;

    private java.lang.String commentFromDispo;

    private java.lang.Float loadingMeter;

    private java.lang.String packingDescription;

    private SimpexLib.MessageService.Attachment attachment;

    private SimpexLib.MessageService.Attachment attachmentFromDispo;

    private java.lang.String refNr;

    private java.lang.String workflow;

    private java.lang.String externalRefNr;

    public Order() {
    }

    public Order(
           SimpexLib.MessageService.AdditionalValues additionalData,
           java.lang.String commentToDispo,
           java.lang.String orderNr,
           SimpexLib.MessageService.OrderType type,
           java.lang.String productName,
           java.lang.Integer amount,
           java.lang.Integer amount2,
           java.lang.String packing,
           java.lang.String packing2,
           java.lang.Double weight,
           java.lang.String hazard,
           java.lang.String trailer,
           java.lang.String container1,
           java.lang.String container2,
           int state,
           java.lang.String orderNrExtern,
           java.lang.Integer displayAtPos,
           java.lang.String name1,
           java.lang.String name2,
           java.lang.String artNr,
           java.lang.String charge,
           java.lang.String deliveryNr,
           java.lang.String commentFromDispo,
           java.lang.Float loadingMeter,
           java.lang.String packingDescription,
           SimpexLib.MessageService.Attachment attachment,
           SimpexLib.MessageService.Attachment attachmentFromDispo,
           java.lang.String refNr,
           java.lang.String workflow,
           java.lang.String externalRefNr) {
           this.additionalData = additionalData;
           this.commentToDispo = commentToDispo;
           this.orderNr = orderNr;
           this.type = type;
           this.productName = productName;
           this.amount = amount;
           this.amount2 = amount2;
           this.packing = packing;
           this.packing2 = packing2;
           this.weight = weight;
           this.hazard = hazard;
           this.trailer = trailer;
           this.container1 = container1;
           this.container2 = container2;
           this.state = state;
           this.orderNrExtern = orderNrExtern;
           this.displayAtPos = displayAtPos;
           this.name1 = name1;
           this.name2 = name2;
           this.artNr = artNr;
           this.charge = charge;
           this.deliveryNr = deliveryNr;
           this.commentFromDispo = commentFromDispo;
           this.loadingMeter = loadingMeter;
           this.packingDescription = packingDescription;
           this.attachment = attachment;
           this.attachmentFromDispo = attachmentFromDispo;
           this.refNr = refNr;
           this.workflow = workflow;
           this.externalRefNr = externalRefNr;
    }


    /**
     * Gets the additionalData value for this Order.
     * 
     * @return additionalData
     */
    public SimpexLib.MessageService.AdditionalValues getAdditionalData() {
        return additionalData;
    }


    /**
     * Sets the additionalData value for this Order.
     * 
     * @param additionalData
     */
    public void setAdditionalData(SimpexLib.MessageService.AdditionalValues additionalData) {
        this.additionalData = additionalData;
    }


    /**
     * Gets the commentToDispo value for this Order.
     * 
     * @return commentToDispo
     */
    public java.lang.String getCommentToDispo() {
        return commentToDispo;
    }


    /**
     * Sets the commentToDispo value for this Order.
     * 
     * @param commentToDispo
     */
    public void setCommentToDispo(java.lang.String commentToDispo) {
        this.commentToDispo = commentToDispo;
    }


    /**
     * Gets the orderNr value for this Order.
     * 
     * @return orderNr
     */
    public java.lang.String getOrderNr() {
        return orderNr;
    }


    /**
     * Sets the orderNr value for this Order.
     * 
     * @param orderNr
     */
    public void setOrderNr(java.lang.String orderNr) {
        this.orderNr = orderNr;
    }


    /**
     * Gets the type value for this Order.
     * 
     * @return type
     */
    public SimpexLib.MessageService.OrderType getType() {
        return type;
    }


    /**
     * Sets the type value for this Order.
     * 
     * @param type
     */
    public void setType(SimpexLib.MessageService.OrderType type) {
        this.type = type;
    }


    /**
     * Gets the productName value for this Order.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this Order.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the amount value for this Order.
     * 
     * @return amount
     */
    public java.lang.Integer getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this Order.
     * 
     * @param amount
     */
    public void setAmount(java.lang.Integer amount) {
        this.amount = amount;
    }


    /**
     * Gets the amount2 value for this Order.
     * 
     * @return amount2
     */
    public java.lang.Integer getAmount2() {
        return amount2;
    }


    /**
     * Sets the amount2 value for this Order.
     * 
     * @param amount2
     */
    public void setAmount2(java.lang.Integer amount2) {
        this.amount2 = amount2;
    }


    /**
     * Gets the packing value for this Order.
     * 
     * @return packing
     */
    public java.lang.String getPacking() {
        return packing;
    }


    /**
     * Sets the packing value for this Order.
     * 
     * @param packing
     */
    public void setPacking(java.lang.String packing) {
        this.packing = packing;
    }


    /**
     * Gets the packing2 value for this Order.
     * 
     * @return packing2
     */
    public java.lang.String getPacking2() {
        return packing2;
    }


    /**
     * Sets the packing2 value for this Order.
     * 
     * @param packing2
     */
    public void setPacking2(java.lang.String packing2) {
        this.packing2 = packing2;
    }


    /**
     * Gets the weight value for this Order.
     * 
     * @return weight
     */
    public java.lang.Double getWeight() {
        return weight;
    }


    /**
     * Sets the weight value for this Order.
     * 
     * @param weight
     */
    public void setWeight(java.lang.Double weight) {
        this.weight = weight;
    }


    /**
     * Gets the hazard value for this Order.
     * 
     * @return hazard
     */
    public java.lang.String getHazard() {
        return hazard;
    }


    /**
     * Sets the hazard value for this Order.
     * 
     * @param hazard
     */
    public void setHazard(java.lang.String hazard) {
        this.hazard = hazard;
    }


    /**
     * Gets the trailer value for this Order.
     * 
     * @return trailer
     */
    public java.lang.String getTrailer() {
        return trailer;
    }


    /**
     * Sets the trailer value for this Order.
     * 
     * @param trailer
     */
    public void setTrailer(java.lang.String trailer) {
        this.trailer = trailer;
    }


    /**
     * Gets the container1 value for this Order.
     * 
     * @return container1
     */
    public java.lang.String getContainer1() {
        return container1;
    }


    /**
     * Sets the container1 value for this Order.
     * 
     * @param container1
     */
    public void setContainer1(java.lang.String container1) {
        this.container1 = container1;
    }


    /**
     * Gets the container2 value for this Order.
     * 
     * @return container2
     */
    public java.lang.String getContainer2() {
        return container2;
    }


    /**
     * Sets the container2 value for this Order.
     * 
     * @param container2
     */
    public void setContainer2(java.lang.String container2) {
        this.container2 = container2;
    }


    /**
     * Gets the state value for this Order.
     * 
     * @return state
     */
    public int getState() {
        return state;
    }


    /**
     * Sets the state value for this Order.
     * 
     * @param state
     */
    public void setState(int state) {
        this.state = state;
    }


    /**
     * Gets the orderNrExtern value for this Order.
     * 
     * @return orderNrExtern
     */
    public java.lang.String getOrderNrExtern() {
        return orderNrExtern;
    }


    /**
     * Sets the orderNrExtern value for this Order.
     * 
     * @param orderNrExtern
     */
    public void setOrderNrExtern(java.lang.String orderNrExtern) {
        this.orderNrExtern = orderNrExtern;
    }


    /**
     * Gets the displayAtPos value for this Order.
     * 
     * @return displayAtPos
     */
    public java.lang.Integer getDisplayAtPos() {
        return displayAtPos;
    }


    /**
     * Sets the displayAtPos value for this Order.
     * 
     * @param displayAtPos
     */
    public void setDisplayAtPos(java.lang.Integer displayAtPos) {
        this.displayAtPos = displayAtPos;
    }


    /**
     * Gets the name1 value for this Order.
     * 
     * @return name1
     */
    public java.lang.String getName1() {
        return name1;
    }


    /**
     * Sets the name1 value for this Order.
     * 
     * @param name1
     */
    public void setName1(java.lang.String name1) {
        this.name1 = name1;
    }


    /**
     * Gets the name2 value for this Order.
     * 
     * @return name2
     */
    public java.lang.String getName2() {
        return name2;
    }


    /**
     * Sets the name2 value for this Order.
     * 
     * @param name2
     */
    public void setName2(java.lang.String name2) {
        this.name2 = name2;
    }


    /**
     * Gets the artNr value for this Order.
     * 
     * @return artNr
     */
    public java.lang.String getArtNr() {
        return artNr;
    }


    /**
     * Sets the artNr value for this Order.
     * 
     * @param artNr
     */
    public void setArtNr(java.lang.String artNr) {
        this.artNr = artNr;
    }


    /**
     * Gets the charge value for this Order.
     * 
     * @return charge
     */
    public java.lang.String getCharge() {
        return charge;
    }


    /**
     * Sets the charge value for this Order.
     * 
     * @param charge
     */
    public void setCharge(java.lang.String charge) {
        this.charge = charge;
    }


    /**
     * Gets the deliveryNr value for this Order.
     * 
     * @return deliveryNr
     */
    public java.lang.String getDeliveryNr() {
        return deliveryNr;
    }


    /**
     * Sets the deliveryNr value for this Order.
     * 
     * @param deliveryNr
     */
    public void setDeliveryNr(java.lang.String deliveryNr) {
        this.deliveryNr = deliveryNr;
    }


    /**
     * Gets the commentFromDispo value for this Order.
     * 
     * @return commentFromDispo
     */
    public java.lang.String getCommentFromDispo() {
        return commentFromDispo;
    }


    /**
     * Sets the commentFromDispo value for this Order.
     * 
     * @param commentFromDispo
     */
    public void setCommentFromDispo(java.lang.String commentFromDispo) {
        this.commentFromDispo = commentFromDispo;
    }


    /**
     * Gets the loadingMeter value for this Order.
     * 
     * @return loadingMeter
     */
    public java.lang.Float getLoadingMeter() {
        return loadingMeter;
    }


    /**
     * Sets the loadingMeter value for this Order.
     * 
     * @param loadingMeter
     */
    public void setLoadingMeter(java.lang.Float loadingMeter) {
        this.loadingMeter = loadingMeter;
    }


    /**
     * Gets the packingDescription value for this Order.
     * 
     * @return packingDescription
     */
    public java.lang.String getPackingDescription() {
        return packingDescription;
    }


    /**
     * Sets the packingDescription value for this Order.
     * 
     * @param packingDescription
     */
    public void setPackingDescription(java.lang.String packingDescription) {
        this.packingDescription = packingDescription;
    }


    /**
     * Gets the attachment value for this Order.
     * 
     * @return attachment
     */
    public SimpexLib.MessageService.Attachment getAttachment() {
        return attachment;
    }


    /**
     * Sets the attachment value for this Order.
     * 
     * @param attachment
     */
    public void setAttachment(SimpexLib.MessageService.Attachment attachment) {
        this.attachment = attachment;
    }


    /**
     * Gets the attachmentFromDispo value for this Order.
     * 
     * @return attachmentFromDispo
     */
    public SimpexLib.MessageService.Attachment getAttachmentFromDispo() {
        return attachmentFromDispo;
    }


    /**
     * Sets the attachmentFromDispo value for this Order.
     * 
     * @param attachmentFromDispo
     */
    public void setAttachmentFromDispo(SimpexLib.MessageService.Attachment attachmentFromDispo) {
        this.attachmentFromDispo = attachmentFromDispo;
    }


    /**
     * Gets the refNr value for this Order.
     * 
     * @return refNr
     */
    public java.lang.String getRefNr() {
        return refNr;
    }


    /**
     * Sets the refNr value for this Order.
     * 
     * @param refNr
     */
    public void setRefNr(java.lang.String refNr) {
        this.refNr = refNr;
    }


    /**
     * Gets the workflow value for this Order.
     * 
     * @return workflow
     */
    public java.lang.String getWorkflow() {
        return workflow;
    }


    /**
     * Sets the workflow value for this Order.
     * 
     * @param workflow
     */
    public void setWorkflow(java.lang.String workflow) {
        this.workflow = workflow;
    }


    /**
     * Gets the externalRefNr value for this Order.
     * 
     * @return externalRefNr
     */
    public java.lang.String getExternalRefNr() {
        return externalRefNr;
    }


    /**
     * Sets the externalRefNr value for this Order.
     * 
     * @param externalRefNr
     */
    public void setExternalRefNr(java.lang.String externalRefNr) {
        this.externalRefNr = externalRefNr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Order)) return false;
        Order other = (Order) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.additionalData==null && other.getAdditionalData()==null) || 
             (this.additionalData!=null &&
              this.additionalData.equals(other.getAdditionalData()))) &&
            ((this.commentToDispo==null && other.getCommentToDispo()==null) || 
             (this.commentToDispo!=null &&
              this.commentToDispo.equals(other.getCommentToDispo()))) &&
            ((this.orderNr==null && other.getOrderNr()==null) || 
             (this.orderNr!=null &&
              this.orderNr.equals(other.getOrderNr()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.amount2==null && other.getAmount2()==null) || 
             (this.amount2!=null &&
              this.amount2.equals(other.getAmount2()))) &&
            ((this.packing==null && other.getPacking()==null) || 
             (this.packing!=null &&
              this.packing.equals(other.getPacking()))) &&
            ((this.packing2==null && other.getPacking2()==null) || 
             (this.packing2!=null &&
              this.packing2.equals(other.getPacking2()))) &&
            ((this.weight==null && other.getWeight()==null) || 
             (this.weight!=null &&
              this.weight.equals(other.getWeight()))) &&
            ((this.hazard==null && other.getHazard()==null) || 
             (this.hazard!=null &&
              this.hazard.equals(other.getHazard()))) &&
            ((this.trailer==null && other.getTrailer()==null) || 
             (this.trailer!=null &&
              this.trailer.equals(other.getTrailer()))) &&
            ((this.container1==null && other.getContainer1()==null) || 
             (this.container1!=null &&
              this.container1.equals(other.getContainer1()))) &&
            ((this.container2==null && other.getContainer2()==null) || 
             (this.container2!=null &&
              this.container2.equals(other.getContainer2()))) &&
            this.state == other.getState() &&
            ((this.orderNrExtern==null && other.getOrderNrExtern()==null) || 
             (this.orderNrExtern!=null &&
              this.orderNrExtern.equals(other.getOrderNrExtern()))) &&
            ((this.displayAtPos==null && other.getDisplayAtPos()==null) || 
             (this.displayAtPos!=null &&
              this.displayAtPos.equals(other.getDisplayAtPos()))) &&
            ((this.name1==null && other.getName1()==null) || 
             (this.name1!=null &&
              this.name1.equals(other.getName1()))) &&
            ((this.name2==null && other.getName2()==null) || 
             (this.name2!=null &&
              this.name2.equals(other.getName2()))) &&
            ((this.artNr==null && other.getArtNr()==null) || 
             (this.artNr!=null &&
              this.artNr.equals(other.getArtNr()))) &&
            ((this.charge==null && other.getCharge()==null) || 
             (this.charge!=null &&
              this.charge.equals(other.getCharge()))) &&
            ((this.deliveryNr==null && other.getDeliveryNr()==null) || 
             (this.deliveryNr!=null &&
              this.deliveryNr.equals(other.getDeliveryNr()))) &&
            ((this.commentFromDispo==null && other.getCommentFromDispo()==null) || 
             (this.commentFromDispo!=null &&
              this.commentFromDispo.equals(other.getCommentFromDispo()))) &&
            ((this.loadingMeter==null && other.getLoadingMeter()==null) || 
             (this.loadingMeter!=null &&
              this.loadingMeter.equals(other.getLoadingMeter()))) &&
            ((this.packingDescription==null && other.getPackingDescription()==null) || 
             (this.packingDescription!=null &&
              this.packingDescription.equals(other.getPackingDescription()))) &&
            ((this.attachment==null && other.getAttachment()==null) || 
             (this.attachment!=null &&
              this.attachment.equals(other.getAttachment()))) &&
            ((this.attachmentFromDispo==null && other.getAttachmentFromDispo()==null) || 
             (this.attachmentFromDispo!=null &&
              this.attachmentFromDispo.equals(other.getAttachmentFromDispo()))) &&
            ((this.refNr==null && other.getRefNr()==null) || 
             (this.refNr!=null &&
              this.refNr.equals(other.getRefNr()))) &&
            ((this.workflow==null && other.getWorkflow()==null) || 
             (this.workflow!=null &&
              this.workflow.equals(other.getWorkflow()))) &&
            ((this.externalRefNr==null && other.getExternalRefNr()==null) || 
             (this.externalRefNr!=null &&
              this.externalRefNr.equals(other.getExternalRefNr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdditionalData() != null) {
            _hashCode += getAdditionalData().hashCode();
        }
        if (getCommentToDispo() != null) {
            _hashCode += getCommentToDispo().hashCode();
        }
        if (getOrderNr() != null) {
            _hashCode += getOrderNr().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getAmount2() != null) {
            _hashCode += getAmount2().hashCode();
        }
        if (getPacking() != null) {
            _hashCode += getPacking().hashCode();
        }
        if (getPacking2() != null) {
            _hashCode += getPacking2().hashCode();
        }
        if (getWeight() != null) {
            _hashCode += getWeight().hashCode();
        }
        if (getHazard() != null) {
            _hashCode += getHazard().hashCode();
        }
        if (getTrailer() != null) {
            _hashCode += getTrailer().hashCode();
        }
        if (getContainer1() != null) {
            _hashCode += getContainer1().hashCode();
        }
        if (getContainer2() != null) {
            _hashCode += getContainer2().hashCode();
        }
        _hashCode += getState();
        if (getOrderNrExtern() != null) {
            _hashCode += getOrderNrExtern().hashCode();
        }
        if (getDisplayAtPos() != null) {
            _hashCode += getDisplayAtPos().hashCode();
        }
        if (getName1() != null) {
            _hashCode += getName1().hashCode();
        }
        if (getName2() != null) {
            _hashCode += getName2().hashCode();
        }
        if (getArtNr() != null) {
            _hashCode += getArtNr().hashCode();
        }
        if (getCharge() != null) {
            _hashCode += getCharge().hashCode();
        }
        if (getDeliveryNr() != null) {
            _hashCode += getDeliveryNr().hashCode();
        }
        if (getCommentFromDispo() != null) {
            _hashCode += getCommentFromDispo().hashCode();
        }
        if (getLoadingMeter() != null) {
            _hashCode += getLoadingMeter().hashCode();
        }
        if (getPackingDescription() != null) {
            _hashCode += getPackingDescription().hashCode();
        }
        if (getAttachment() != null) {
            _hashCode += getAttachment().hashCode();
        }
        if (getAttachmentFromDispo() != null) {
            _hashCode += getAttachmentFromDispo().hashCode();
        }
        if (getRefNr() != null) {
            _hashCode += getRefNr().hashCode();
        }
        if (getWorkflow() != null) {
            _hashCode += getWorkflow().hashCode();
        }
        if (getExternalRefNr() != null) {
            _hashCode += getExternalRefNr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Order.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Order"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AdditionalValues"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentToDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CommentToDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "OrderNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "OrderType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ProductName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Amount2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packing");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Packing"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packing2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Packing2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Weight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hazard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Hazard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trailer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Trailer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("container1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Container1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("container2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Container2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNrExtern");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "OrderNrExtern"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayAtPos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DisplayAtPos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Name1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Name2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("artNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ArtNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("charge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Charge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DeliveryNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CommentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loadingMeter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "LoadingMeter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packingDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PackingDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFromDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachmentFromDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Attachment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RefNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Workflow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalRefNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ExternalRefNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
