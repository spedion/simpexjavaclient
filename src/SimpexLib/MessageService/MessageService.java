/**
 * MessageService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public interface MessageService extends javax.xml.rpc.Service {
    public java.lang.String getMessageServiceSoapAddress();

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap() throws javax.xml.rpc.ServiceException;

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getMessageServiceSoap12Address();

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap12() throws javax.xml.rpc.ServiceException;

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
