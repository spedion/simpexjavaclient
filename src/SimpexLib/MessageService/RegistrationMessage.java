/**
 * RegistrationMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class RegistrationMessage  implements java.io.Serializable {
    private java.lang.String referenceNr;

    private java.lang.String vehicle;

    private SimpexLib.MessageService.Driver driver;

    private java.lang.String driverMobilePhoneNr;

    private java.lang.String driverEmailAdress;

    private java.lang.String comment;

    private java.lang.String dispoPhoneNr;

    private java.lang.Integer customerId;

    private boolean useDummyDriverIfDriverIsNull;

    private java.lang.String languageCode;

    private java.lang.Integer userExpiresTimespanInSeconds;

    private SimpexLib.MessageService.RegistrationMessageTextType textStyle;

    public RegistrationMessage() {
    }

    public RegistrationMessage(
           java.lang.String referenceNr,
           java.lang.String vehicle,
           SimpexLib.MessageService.Driver driver,
           java.lang.String driverMobilePhoneNr,
           java.lang.String driverEmailAdress,
           java.lang.String comment,
           java.lang.String dispoPhoneNr,
           java.lang.Integer customerId,
           boolean useDummyDriverIfDriverIsNull,
           java.lang.String languageCode,
           java.lang.Integer userExpiresTimespanInSeconds,
           SimpexLib.MessageService.RegistrationMessageTextType textStyle) {
           this.referenceNr = referenceNr;
           this.vehicle = vehicle;
           this.driver = driver;
           this.driverMobilePhoneNr = driverMobilePhoneNr;
           this.driverEmailAdress = driverEmailAdress;
           this.comment = comment;
           this.dispoPhoneNr = dispoPhoneNr;
           this.customerId = customerId;
           this.useDummyDriverIfDriverIsNull = useDummyDriverIfDriverIsNull;
           this.languageCode = languageCode;
           this.userExpiresTimespanInSeconds = userExpiresTimespanInSeconds;
           this.textStyle = textStyle;
    }


    /**
     * Gets the referenceNr value for this RegistrationMessage.
     * 
     * @return referenceNr
     */
    public java.lang.String getReferenceNr() {
        return referenceNr;
    }


    /**
     * Sets the referenceNr value for this RegistrationMessage.
     * 
     * @param referenceNr
     */
    public void setReferenceNr(java.lang.String referenceNr) {
        this.referenceNr = referenceNr;
    }


    /**
     * Gets the vehicle value for this RegistrationMessage.
     * 
     * @return vehicle
     */
    public java.lang.String getVehicle() {
        return vehicle;
    }


    /**
     * Sets the vehicle value for this RegistrationMessage.
     * 
     * @param vehicle
     */
    public void setVehicle(java.lang.String vehicle) {
        this.vehicle = vehicle;
    }


    /**
     * Gets the driver value for this RegistrationMessage.
     * 
     * @return driver
     */
    public SimpexLib.MessageService.Driver getDriver() {
        return driver;
    }


    /**
     * Sets the driver value for this RegistrationMessage.
     * 
     * @param driver
     */
    public void setDriver(SimpexLib.MessageService.Driver driver) {
        this.driver = driver;
    }


    /**
     * Gets the driverMobilePhoneNr value for this RegistrationMessage.
     * 
     * @return driverMobilePhoneNr
     */
    public java.lang.String getDriverMobilePhoneNr() {
        return driverMobilePhoneNr;
    }


    /**
     * Sets the driverMobilePhoneNr value for this RegistrationMessage.
     * 
     * @param driverMobilePhoneNr
     */
    public void setDriverMobilePhoneNr(java.lang.String driverMobilePhoneNr) {
        this.driverMobilePhoneNr = driverMobilePhoneNr;
    }


    /**
     * Gets the driverEmailAdress value for this RegistrationMessage.
     * 
     * @return driverEmailAdress
     */
    public java.lang.String getDriverEmailAdress() {
        return driverEmailAdress;
    }


    /**
     * Sets the driverEmailAdress value for this RegistrationMessage.
     * 
     * @param driverEmailAdress
     */
    public void setDriverEmailAdress(java.lang.String driverEmailAdress) {
        this.driverEmailAdress = driverEmailAdress;
    }


    /**
     * Gets the comment value for this RegistrationMessage.
     * 
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this RegistrationMessage.
     * 
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }


    /**
     * Gets the dispoPhoneNr value for this RegistrationMessage.
     * 
     * @return dispoPhoneNr
     */
    public java.lang.String getDispoPhoneNr() {
        return dispoPhoneNr;
    }


    /**
     * Sets the dispoPhoneNr value for this RegistrationMessage.
     * 
     * @param dispoPhoneNr
     */
    public void setDispoPhoneNr(java.lang.String dispoPhoneNr) {
        this.dispoPhoneNr = dispoPhoneNr;
    }


    /**
     * Gets the customerId value for this RegistrationMessage.
     * 
     * @return customerId
     */
    public java.lang.Integer getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this RegistrationMessage.
     * 
     * @param customerId
     */
    public void setCustomerId(java.lang.Integer customerId) {
        this.customerId = customerId;
    }


    /**
     * Gets the useDummyDriverIfDriverIsNull value for this RegistrationMessage.
     * 
     * @return useDummyDriverIfDriverIsNull
     */
    public boolean isUseDummyDriverIfDriverIsNull() {
        return useDummyDriverIfDriverIsNull;
    }


    /**
     * Sets the useDummyDriverIfDriverIsNull value for this RegistrationMessage.
     * 
     * @param useDummyDriverIfDriverIsNull
     */
    public void setUseDummyDriverIfDriverIsNull(boolean useDummyDriverIfDriverIsNull) {
        this.useDummyDriverIfDriverIsNull = useDummyDriverIfDriverIsNull;
    }


    /**
     * Gets the languageCode value for this RegistrationMessage.
     * 
     * @return languageCode
     */
    public java.lang.String getLanguageCode() {
        return languageCode;
    }


    /**
     * Sets the languageCode value for this RegistrationMessage.
     * 
     * @param languageCode
     */
    public void setLanguageCode(java.lang.String languageCode) {
        this.languageCode = languageCode;
    }


    /**
     * Gets the userExpiresTimespanInSeconds value for this RegistrationMessage.
     * 
     * @return userExpiresTimespanInSeconds
     */
    public java.lang.Integer getUserExpiresTimespanInSeconds() {
        return userExpiresTimespanInSeconds;
    }


    /**
     * Sets the userExpiresTimespanInSeconds value for this RegistrationMessage.
     * 
     * @param userExpiresTimespanInSeconds
     */
    public void setUserExpiresTimespanInSeconds(java.lang.Integer userExpiresTimespanInSeconds) {
        this.userExpiresTimespanInSeconds = userExpiresTimespanInSeconds;
    }


    /**
     * Gets the textStyle value for this RegistrationMessage.
     * 
     * @return textStyle
     */
    public SimpexLib.MessageService.RegistrationMessageTextType getTextStyle() {
        return textStyle;
    }


    /**
     * Sets the textStyle value for this RegistrationMessage.
     * 
     * @param textStyle
     */
    public void setTextStyle(SimpexLib.MessageService.RegistrationMessageTextType textStyle) {
        this.textStyle = textStyle;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistrationMessage)) return false;
        RegistrationMessage other = (RegistrationMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.referenceNr==null && other.getReferenceNr()==null) || 
             (this.referenceNr!=null &&
              this.referenceNr.equals(other.getReferenceNr()))) &&
            ((this.vehicle==null && other.getVehicle()==null) || 
             (this.vehicle!=null &&
              this.vehicle.equals(other.getVehicle()))) &&
            ((this.driver==null && other.getDriver()==null) || 
             (this.driver!=null &&
              this.driver.equals(other.getDriver()))) &&
            ((this.driverMobilePhoneNr==null && other.getDriverMobilePhoneNr()==null) || 
             (this.driverMobilePhoneNr!=null &&
              this.driverMobilePhoneNr.equals(other.getDriverMobilePhoneNr()))) &&
            ((this.driverEmailAdress==null && other.getDriverEmailAdress()==null) || 
             (this.driverEmailAdress!=null &&
              this.driverEmailAdress.equals(other.getDriverEmailAdress()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.dispoPhoneNr==null && other.getDispoPhoneNr()==null) || 
             (this.dispoPhoneNr!=null &&
              this.dispoPhoneNr.equals(other.getDispoPhoneNr()))) &&
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId()))) &&
            this.useDummyDriverIfDriverIsNull == other.isUseDummyDriverIfDriverIsNull() &&
            ((this.languageCode==null && other.getLanguageCode()==null) || 
             (this.languageCode!=null &&
              this.languageCode.equals(other.getLanguageCode()))) &&
            ((this.userExpiresTimespanInSeconds==null && other.getUserExpiresTimespanInSeconds()==null) || 
             (this.userExpiresTimespanInSeconds!=null &&
              this.userExpiresTimespanInSeconds.equals(other.getUserExpiresTimespanInSeconds()))) &&
            ((this.textStyle==null && other.getTextStyle()==null) || 
             (this.textStyle!=null &&
              this.textStyle.equals(other.getTextStyle())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReferenceNr() != null) {
            _hashCode += getReferenceNr().hashCode();
        }
        if (getVehicle() != null) {
            _hashCode += getVehicle().hashCode();
        }
        if (getDriver() != null) {
            _hashCode += getDriver().hashCode();
        }
        if (getDriverMobilePhoneNr() != null) {
            _hashCode += getDriverMobilePhoneNr().hashCode();
        }
        if (getDriverEmailAdress() != null) {
            _hashCode += getDriverEmailAdress().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getDispoPhoneNr() != null) {
            _hashCode += getDispoPhoneNr().hashCode();
        }
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        _hashCode += (isUseDummyDriverIfDriverIsNull() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getLanguageCode() != null) {
            _hashCode += getLanguageCode().hashCode();
        }
        if (getUserExpiresTimespanInSeconds() != null) {
            _hashCode += getUserExpiresTimespanInSeconds().hashCode();
        }
        if (getTextStyle() != null) {
            _hashCode += getTextStyle().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistrationMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RegistrationMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ReferenceNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Vehicle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Driver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Driver"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driverMobilePhoneNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DriverMobilePhoneNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driverEmailAdress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DriverEmailAdress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dispoPhoneNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DispoPhoneNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "CustomerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("useDummyDriverIfDriverIsNull");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UseDummyDriverIfDriverIsNull"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("languageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "LanguageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userExpiresTimespanInSeconds");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UserExpiresTimespanInSeconds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("textStyle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "TextStyle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "RegistrationMessageTextType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
