/**
 * MessageServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class MessageServiceLocator extends org.apache.axis.client.Service implements SimpexLib.MessageService.MessageService {

    public MessageServiceLocator() {
    }


    public MessageServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MessageServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MessageServiceSoap
    private java.lang.String MessageServiceSoap_address = "https://ws2.spedion.de/simpex/3.1/Services/MessageService.asmx";

    public java.lang.String getMessageServiceSoapAddress() {
        return MessageServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MessageServiceSoapWSDDServiceName = "MessageServiceSoap";

    public java.lang.String getMessageServiceSoapWSDDServiceName() {
        return MessageServiceSoapWSDDServiceName;
    }

    public void setMessageServiceSoapWSDDServiceName(java.lang.String name) {
        MessageServiceSoapWSDDServiceName = name;
    }

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MessageServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMessageServiceSoap(endpoint);
    }

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            SimpexLib.MessageService.MessageServiceSoapStub _stub = new SimpexLib.MessageService.MessageServiceSoapStub(portAddress, this);
            _stub.setPortName(getMessageServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMessageServiceSoapEndpointAddress(java.lang.String address) {
        MessageServiceSoap_address = address;
    }


    // Use to get a proxy class for MessageServiceSoap12
    private java.lang.String MessageServiceSoap12_address = "https://ws2.spedion.de/simpex/3.1/Services/MessageService.asmx";

    public java.lang.String getMessageServiceSoap12Address() {
        return MessageServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MessageServiceSoap12WSDDServiceName = "MessageServiceSoap12";

    public java.lang.String getMessageServiceSoap12WSDDServiceName() {
        return MessageServiceSoap12WSDDServiceName;
    }

    public void setMessageServiceSoap12WSDDServiceName(java.lang.String name) {
        MessageServiceSoap12WSDDServiceName = name;
    }

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MessageServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMessageServiceSoap12(endpoint);
    }

    public SimpexLib.MessageService.MessageServiceSoap getMessageServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            SimpexLib.MessageService.MessageServiceSoap12Stub _stub = new SimpexLib.MessageService.MessageServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getMessageServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMessageServiceSoap12EndpointAddress(java.lang.String address) {
        MessageServiceSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (SimpexLib.MessageService.MessageServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                SimpexLib.MessageService.MessageServiceSoapStub _stub = new SimpexLib.MessageService.MessageServiceSoapStub(new java.net.URL(MessageServiceSoap_address), this);
                _stub.setPortName(getMessageServiceSoapWSDDServiceName());
                return _stub;
            }
            if (SimpexLib.MessageService.MessageServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                SimpexLib.MessageService.MessageServiceSoap12Stub _stub = new SimpexLib.MessageService.MessageServiceSoap12Stub(new java.net.URL(MessageServiceSoap12_address), this);
                _stub.setPortName(getMessageServiceSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MessageServiceSoap".equals(inputPortName)) {
            return getMessageServiceSoap();
        }
        else if ("MessageServiceSoap12".equals(inputPortName)) {
            return getMessageServiceSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MessageService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MessageServiceSoap"));
            ports.add(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "MessageServiceSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MessageServiceSoap".equals(portName)) {
            setMessageServiceSoapEndpointAddress(address);
        }
        else 
if ("MessageServiceSoap12".equals(portName)) {
            setMessageServiceSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
