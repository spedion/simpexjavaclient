/**
 * PresignedUploadUrlResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class PresignedUploadUrlResult  implements java.io.Serializable {
    private SimpexLib.MessageService.ResultCode resultCode;

    private SimpexLib.MessageService.ErrorCode errorCode;

    private java.lang.String attachmentUrl;

    private java.lang.String uploadUrl;

    private java.lang.String filename;

    public PresignedUploadUrlResult() {
    }

    public PresignedUploadUrlResult(
           SimpexLib.MessageService.ResultCode resultCode,
           SimpexLib.MessageService.ErrorCode errorCode,
           java.lang.String attachmentUrl,
           java.lang.String uploadUrl,
           java.lang.String filename) {
           this.resultCode = resultCode;
           this.errorCode = errorCode;
           this.attachmentUrl = attachmentUrl;
           this.uploadUrl = uploadUrl;
           this.filename = filename;
    }


    /**
     * Gets the resultCode value for this PresignedUploadUrlResult.
     * 
     * @return resultCode
     */
    public SimpexLib.MessageService.ResultCode getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this PresignedUploadUrlResult.
     * 
     * @param resultCode
     */
    public void setResultCode(SimpexLib.MessageService.ResultCode resultCode) {
        this.resultCode = resultCode;
    }


    /**
     * Gets the errorCode value for this PresignedUploadUrlResult.
     * 
     * @return errorCode
     */
    public SimpexLib.MessageService.ErrorCode getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this PresignedUploadUrlResult.
     * 
     * @param errorCode
     */
    public void setErrorCode(SimpexLib.MessageService.ErrorCode errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the attachmentUrl value for this PresignedUploadUrlResult.
     * 
     * @return attachmentUrl
     */
    public java.lang.String getAttachmentUrl() {
        return attachmentUrl;
    }


    /**
     * Sets the attachmentUrl value for this PresignedUploadUrlResult.
     * 
     * @param attachmentUrl
     */
    public void setAttachmentUrl(java.lang.String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }


    /**
     * Gets the uploadUrl value for this PresignedUploadUrlResult.
     * 
     * @return uploadUrl
     */
    public java.lang.String getUploadUrl() {
        return uploadUrl;
    }


    /**
     * Sets the uploadUrl value for this PresignedUploadUrlResult.
     * 
     * @param uploadUrl
     */
    public void setUploadUrl(java.lang.String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }


    /**
     * Gets the filename value for this PresignedUploadUrlResult.
     * 
     * @return filename
     */
    public java.lang.String getFilename() {
        return filename;
    }


    /**
     * Sets the filename value for this PresignedUploadUrlResult.
     * 
     * @param filename
     */
    public void setFilename(java.lang.String filename) {
        this.filename = filename;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PresignedUploadUrlResult)) return false;
        PresignedUploadUrlResult other = (PresignedUploadUrlResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.attachmentUrl==null && other.getAttachmentUrl()==null) || 
             (this.attachmentUrl!=null &&
              this.attachmentUrl.equals(other.getAttachmentUrl()))) &&
            ((this.uploadUrl==null && other.getUploadUrl()==null) || 
             (this.uploadUrl!=null &&
              this.uploadUrl.equals(other.getUploadUrl()))) &&
            ((this.filename==null && other.getFilename()==null) || 
             (this.filename!=null &&
              this.filename.equals(other.getFilename())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getAttachmentUrl() != null) {
            _hashCode += getAttachmentUrl().hashCode();
        }
        if (getUploadUrl() != null) {
            _hashCode += getUploadUrl().hashCode();
        }
        if (getFilename() != null) {
            _hashCode += getFilename().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PresignedUploadUrlResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "PresignedUploadUrlResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ResultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ResultCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ErrorCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "AttachmentUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uploadUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "UploadUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "Filename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
