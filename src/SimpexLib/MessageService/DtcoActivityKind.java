/**
 * DtcoActivityKind.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class DtcoActivityKind implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected DtcoActivityKind(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Work = "Work";
    public static final java.lang.String _Pause = "Pause";
    public static final java.lang.String _Rest = "Rest";
    public static final java.lang.String _Drive = "Drive";
    public static final java.lang.String _Standby = "Standby";
    public static final java.lang.String _Unknown = "Unknown";
    public static final java.lang.String _Ignore = "Ignore";
    public static final DtcoActivityKind Work = new DtcoActivityKind(_Work);
    public static final DtcoActivityKind Pause = new DtcoActivityKind(_Pause);
    public static final DtcoActivityKind Rest = new DtcoActivityKind(_Rest);
    public static final DtcoActivityKind Drive = new DtcoActivityKind(_Drive);
    public static final DtcoActivityKind Standby = new DtcoActivityKind(_Standby);
    public static final DtcoActivityKind Unknown = new DtcoActivityKind(_Unknown);
    public static final DtcoActivityKind Ignore = new DtcoActivityKind(_Ignore);
    public java.lang.String getValue() { return _value_;}
    public static DtcoActivityKind fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        DtcoActivityKind enumeration = (DtcoActivityKind)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static DtcoActivityKind fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DtcoActivityKind.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "DtcoActivityKind"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
