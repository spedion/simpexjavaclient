/**
 * MessageServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public interface MessageServiceSoap extends java.rmi.Remote {
    public int getMessageCount() throws java.rmi.RemoteException;
    public boolean parseIdentifierString(java.lang.String vehicleName) throws java.rmi.RemoteException;
    public boolean parseIdentifierStringSubCustomer(java.lang.String vehicleName, int subCustomerId) throws java.rmi.RemoteException;
    public void getUnreadMessage(SimpexLib.MessageService.holders.ArrayOfMessageHolder getUnreadMessageResult, SimpexLib.MessageService.holders.ResultHolder r) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Message[] getNewMessage() throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Message getMessageId(java.math.BigDecimal messageId) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result addMessage(SimpexLib.MessageService.Message message) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.PresignedUploadUrlResult getPresignedUploadUrl(java.lang.String filename, long filesize) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursByVehicle(java.lang.String vehicleName, int skipCount, int takeCount) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursByVehicleAndState(java.lang.String vehicleName, int skipCount, int takeCount, int minState, int maxState) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursBySubCustomerVehicle(int subCustomerId, java.lang.String vehicleName, int skipCount, int takeCount) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result addRegistrationMessage(SimpexLib.MessageService.RegistrationMessage m) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursByExample(SimpexLib.MessageService.Tour example) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour getTourByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.CustomerTour getCustomerTourForAllSubCustomersByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour getTourByTourNrOfCustomer(java.lang.String tourNr, int customer) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursByPlaceNrs(java.lang.String[] placeNrs) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursByOrderNrs(java.lang.String[] orderNrs) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Tour[] getToursByOrderNrExtern(java.lang.String orderNrExtern) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result deleteTour(java.lang.String tourNr) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result deleteSubCustomerTour(java.lang.String tourNr, int subCustomerId) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result abandonTourByTourNr(java.lang.String tourNr) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result deleteTourObject(SimpexLib.MessageService.Tour wsTour) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result updateTour(SimpexLib.MessageService.Tour newTourdata) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result updateSubCustomerTour(SimpexLib.MessageService.Tour newTourdata, int subCustomerId) throws java.rmi.RemoteException;
    public void reportError(java.lang.String errorReport) throws java.rmi.RemoteException;
    public SimpexLib.MessageService.Result setListOfMessageAsRead(java.math.BigDecimal[] messageIdList) throws java.rmi.RemoteException;
    public java.lang.String getVersion() throws java.rmi.RemoteException;
}
