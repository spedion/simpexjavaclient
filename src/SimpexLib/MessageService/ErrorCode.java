/**
 * ErrorCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService;

public class ErrorCode implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ErrorCode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _OtherError = "OtherError";
    public static final java.lang.String _TemporalFailure = "TemporalFailure";
    public static final java.lang.String _OkCode = "OkCode";
    public static final java.lang.String _UnknownVehicle = "UnknownVehicle";
    public static final java.lang.String _TourAlreadyPresent = "TourAlreadyPresent";
    public static final java.lang.String _TourUnknown = "TourUnknown";
    public static final java.lang.String _TourDuplicate = "TourDuplicate";
    public static final java.lang.String _TourPlaceNotDeletable = "TourPlaceNotDeletable";
    public static final java.lang.String _TourNotMovableToOtherVehicle = "TourNotMovableToOtherVehicle";
    public static final java.lang.String _TourNotDeletable = "TourNotDeletable";
    public static final java.lang.String _TourPlaceDuplicated = "TourPlaceDuplicated";
    public static final java.lang.String _TourPlaceNotFound = "TourPlaceNotFound";
    public static final java.lang.String _TourOrderDuplicated = "TourOrderDuplicated";
    public static final java.lang.String _TourOrderNotFound = "TourOrderNotFound";
    public static final java.lang.String _TourPlaceUpdateRejected = "TourPlaceUpdateRejected";
    public static final java.lang.String _TourPlaceInsertFailure = "TourPlaceInsertFailure";
    public static final java.lang.String _TourUpdateRejected = "TourUpdateRejected";
    public static final java.lang.String _TournrInvalid = "TournrInvalid";
    public static final java.lang.String _TourUpdateError = "TourUpdateError";
    public static final java.lang.String _TourDateInvalid = "TourDateInvalid";
    public static final java.lang.String _FeatureNotImplemented = "FeatureNotImplemented";
    public static final java.lang.String _FeatureIsDeprecated = "FeatureIsDeprecated";
    public static final java.lang.String _AdditionalValueItemKeyInvalid = "AdditionalValueItemKeyInvalid";
    public static final java.lang.String _DriverUnknown = "DriverUnknown";
    public static final java.lang.String _DriverInvalidFirstName = "DriverInvalidFirstName";
    public static final java.lang.String _DriverInvalidLastName = "DriverInvalidLastName";
    public static final java.lang.String _DriverInvalidPinIsNull = "DriverInvalidPinIsNull";
    public static final java.lang.String _DriverInvalidPinTooShort = "DriverInvalidPinTooShort";
    public static final java.lang.String _DriverInvalidPinNotPositiveInt = "DriverInvalidPinNotPositiveInt";
    public static final java.lang.String _DriverInvalidCustomerIdNotPositiveInt = "DriverInvalidCustomerIdNotPositiveInt";
    public static final java.lang.String _DriveNoPhoneNumber = "DriveNoPhoneNumber";
    public static final java.lang.String _MessageIsNull = "MessageIsNull";
    public static final java.lang.String _MessageFormInvalid = "MessageFormInvalid";
    public static final java.lang.String _AttachmentBase64SizeToBig = "AttachmentBase64SizeToBig";
    public static final java.lang.String _AttachmentBase64CountLimitExceeded = "AttachmentBase64CountLimitExceeded";
    public static final java.lang.String _AttachmentUrlbasedWrongUrlSchema = "AttachmentUrlbasedWrongUrlSchema";
    public static final java.lang.String _AttachmentBase64DataMissing = "AttachmentBase64DataMissing";
    public static final java.lang.String _AttachmentDirectionError = "AttachmentDirectionError";
    public static final java.lang.String _QueueDoesNotExist = "QueueDoesNotExist";
    public static final java.lang.String _QueueIsNotUniqueForUser = "QueueIsNotUniqueForUser";
    public static final ErrorCode OtherError = new ErrorCode(_OtherError);
    public static final ErrorCode TemporalFailure = new ErrorCode(_TemporalFailure);
    public static final ErrorCode OkCode = new ErrorCode(_OkCode);
    public static final ErrorCode UnknownVehicle = new ErrorCode(_UnknownVehicle);
    public static final ErrorCode TourAlreadyPresent = new ErrorCode(_TourAlreadyPresent);
    public static final ErrorCode TourUnknown = new ErrorCode(_TourUnknown);
    public static final ErrorCode TourDuplicate = new ErrorCode(_TourDuplicate);
    public static final ErrorCode TourPlaceNotDeletable = new ErrorCode(_TourPlaceNotDeletable);
    public static final ErrorCode TourNotMovableToOtherVehicle = new ErrorCode(_TourNotMovableToOtherVehicle);
    public static final ErrorCode TourNotDeletable = new ErrorCode(_TourNotDeletable);
    public static final ErrorCode TourPlaceDuplicated = new ErrorCode(_TourPlaceDuplicated);
    public static final ErrorCode TourPlaceNotFound = new ErrorCode(_TourPlaceNotFound);
    public static final ErrorCode TourOrderDuplicated = new ErrorCode(_TourOrderDuplicated);
    public static final ErrorCode TourOrderNotFound = new ErrorCode(_TourOrderNotFound);
    public static final ErrorCode TourPlaceUpdateRejected = new ErrorCode(_TourPlaceUpdateRejected);
    public static final ErrorCode TourPlaceInsertFailure = new ErrorCode(_TourPlaceInsertFailure);
    public static final ErrorCode TourUpdateRejected = new ErrorCode(_TourUpdateRejected);
    public static final ErrorCode TournrInvalid = new ErrorCode(_TournrInvalid);
    public static final ErrorCode TourUpdateError = new ErrorCode(_TourUpdateError);
    public static final ErrorCode TourDateInvalid = new ErrorCode(_TourDateInvalid);
    public static final ErrorCode FeatureNotImplemented = new ErrorCode(_FeatureNotImplemented);
    public static final ErrorCode FeatureIsDeprecated = new ErrorCode(_FeatureIsDeprecated);
    public static final ErrorCode AdditionalValueItemKeyInvalid = new ErrorCode(_AdditionalValueItemKeyInvalid);
    public static final ErrorCode DriverUnknown = new ErrorCode(_DriverUnknown);
    public static final ErrorCode DriverInvalidFirstName = new ErrorCode(_DriverInvalidFirstName);
    public static final ErrorCode DriverInvalidLastName = new ErrorCode(_DriverInvalidLastName);
    public static final ErrorCode DriverInvalidPinIsNull = new ErrorCode(_DriverInvalidPinIsNull);
    public static final ErrorCode DriverInvalidPinTooShort = new ErrorCode(_DriverInvalidPinTooShort);
    public static final ErrorCode DriverInvalidPinNotPositiveInt = new ErrorCode(_DriverInvalidPinNotPositiveInt);
    public static final ErrorCode DriverInvalidCustomerIdNotPositiveInt = new ErrorCode(_DriverInvalidCustomerIdNotPositiveInt);
    public static final ErrorCode DriveNoPhoneNumber = new ErrorCode(_DriveNoPhoneNumber);
    public static final ErrorCode MessageIsNull = new ErrorCode(_MessageIsNull);
    public static final ErrorCode MessageFormInvalid = new ErrorCode(_MessageFormInvalid);
    public static final ErrorCode AttachmentBase64SizeToBig = new ErrorCode(_AttachmentBase64SizeToBig);
    public static final ErrorCode AttachmentBase64CountLimitExceeded = new ErrorCode(_AttachmentBase64CountLimitExceeded);
    public static final ErrorCode AttachmentUrlbasedWrongUrlSchema = new ErrorCode(_AttachmentUrlbasedWrongUrlSchema);
    public static final ErrorCode AttachmentBase64DataMissing = new ErrorCode(_AttachmentBase64DataMissing);
    public static final ErrorCode AttachmentDirectionError = new ErrorCode(_AttachmentDirectionError);
    public static final ErrorCode QueueDoesNotExist = new ErrorCode(_QueueDoesNotExist);
    public static final ErrorCode QueueIsNotUniqueForUser = new ErrorCode(_QueueIsNotUniqueForUser);
    public java.lang.String getValue() { return _value_;}
    public static ErrorCode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ErrorCode enumeration = (ErrorCode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ErrorCode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ErrorCode.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.spedion.de/simpex/0.50", "ErrorCode"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
