/**
 * ArrayOfMessageHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SimpexLib.MessageService.holders;

public final class ArrayOfMessageHolder implements javax.xml.rpc.holders.Holder {
    public SimpexLib.MessageService.Message[] value;

    public ArrayOfMessageHolder() {
    }

    public ArrayOfMessageHolder(SimpexLib.MessageService.Message[] value) {
        this.value = value;
    }

}
