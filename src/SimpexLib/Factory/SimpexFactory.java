package SimpexLib.Factory;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

import SimpexLib.MessageService.DtcoActivityKind;
import SimpexLib.MessageService.Message;
import SimpexLib.MessageService.Order;
import SimpexLib.MessageService.OrderType;
import SimpexLib.MessageService.Place;
import SimpexLib.MessageService.Tour;

public class SimpexFactory {

	public SimpexFactory() {

	}

	public static Message createMessage() {
		Message msg = new Message();
		msg.setMessageTimeUtc(Calendar.getInstance());
		return msg;
	}

	public static Tour createDemoTour() {
		Tour tour = new Tour();

		Order order1 = new Order();
		order1.setAmount(10);
		order1.setCommentFromDispo("SPEDION Bluetoothadapter sicher lagern");
		order1.setProductName("SPEDION Bluetoothadapter");
		order1.setArtNr("1234");
		order1.setOrderNr("11223344");
		order1.setType(OrderType.Load);
		order1.setPacking("EP");
		order1.setWeight(2500.0);

		Order order2 = new Order();
		order2.setAmount(10);
		order2.setCommentFromDispo("SPEDION Kabelsatz sicher lagern");
		order2.setProductName("SPEDION Kabelsatz");
		order2.setArtNr("4321");
		order2.setOrderNr("44332211");
		order2.setType(OrderType.Load);
		order2.setPacking("EP");
		order2.setWeight(5200.0);

		Place p1 = new Place();
		p1.setOrders(new Order[] { order1, order2 });
		p1.setCommentFromDispo("Achtung bissiger Hund!");
		p1.setPlaceNr("1");
		p1.setZip("63829");
		p1.setNation("DE");
		p1.setHousenr("7");
		p1.setAddress("Industriestr.");
		p1.setLocation("Krombach");
		p1.setLatitude(50.0711335);
		p1.setLongitude(9.2184895);
		p1.setDisplayAtPos(1);
		Calendar cal1 = GregorianCalendar.getInstance();
		cal1.set(2016, 12, 31, 10, 30, 00);
		p1.setPlanBeginUtc(cal1);
		Calendar cal2 = GregorianCalendar.getInstance();
		cal2.set(2016, 12, 31, 11, 30, 00);
		p1.setPlanEndUtc(cal2);

		Order order3 = new Order();
		order3.setAmount(10);
		order3.setCommentFromDispo("SPEDION Bluetoothadapter sicher lagern");
		order3.setProductName("SPEDION Bluetoothadapter");
		order3.setArtNr("1234");
		order3.setOrderNr("11223344");
		order3.setType(OrderType.Unload);
		order3.setPacking("EP");
		order3.setWeight(2500.0);

		Order order4 = new Order();
		order4.setAmount(10);
		order4.setCommentFromDispo("SPEDION Kabelsatz sicher lagern");
		order4.setProductName("SPEDION Kabelsatz");
		order4.setArtNr("4321");
		order4.setOrderNr("44332211");
		order4.setType(OrderType.Unload);
		order4.setPacking("EP");
		order4.setWeight(5200.0);

		Place p2 = new Place();
		p2.setOrders(new Order[] { order3, order4 });
		p2.setCommentFromDispo("Achtung lustiger Lagerarbeiter!");
		p2.setPlaceNr("2");
		p2.setZip("21423");
		p2.setNation("DE");
		p2.setHousenr("16");
		p2.setAddress("Sperberweg");
		p2.setLocation("Winsen (Luhe)");
		p2.setLatitude(53.346212);
		p2.setLongitude(10.245073);
		p2.setDisplayAtPos(2);
		Calendar cal3 = GregorianCalendar.getInstance();
		cal3.set(2017, 1, 1, 6, 30, 00);
		p2.setPlanBeginUtc(cal3);
		Calendar cal4 = GregorianCalendar.getInstance();
		cal4.set(2017, 1, 1, 8, 30, 00);
		p2.setPlanEndUtc(cal4);

		Random rand = new Random(Calendar.getInstance().getTimeInMillis());
		tour.setPlaces(new Place[] { p1, p2 });
		tour.setTournr(String.valueOf(rand.nextInt(10000000)));
		tour.setTourDateUtc(Calendar.getInstance());

		return tour;
	}
}
