package SimpexLib.Compression;

import org.apache.axis.EngineConfiguration;
import org.apache.axis.EngineConfigurationFactory;
import org.apache.axis.SimpleTargetedChain;
import org.apache.axis.configuration.BasicClientConfig;
import org.apache.axis.transport.http.CommonsHTTPSender;

public class CompressedEngineConfigurationFactory implements EngineConfigurationFactory {

	public static EngineConfigurationFactory newFactory(Object param) {
	  return new CompressedEngineConfigurationFactory();
	}
	
	public EngineConfiguration getClientEngineConfig() {
	  BasicClientConfig cfg = new BasicClientConfig();
	  cfg.deployTransport("http", new SimpleTargetedChain(new CommonsHTTPSender()));
	  return cfg;
	}
	
	public EngineConfiguration getServerEngineConfig() {
	  return null;
	}
}