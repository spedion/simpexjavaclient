package test;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.Call;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;

import org.apache.axis.AxisProperties;
import org.apache.axis.EngineConfigurationFactory;
import org.apache.axis.transport.http.HTTPConstants;

import SimpexLib.Compression.CompressedEngineConfigurationFactory;
import SimpexLib.Factory.SimpexFactory;
import SimpexLib.MessageService.Message;
import SimpexLib.MessageService.MessageServiceLocator;
import SimpexLib.MessageService.MessageServiceSoap;
import SimpexLib.MessageService.Result;

public class SimpexTest {

	public static void main(String[] args) {
		if (args != null && args.length == 3) {
			SimpexTest test = new SimpexTest(args[0], args[1], args[2]);
		} else {
			System.out.println(
					"usage: java -jar SimpexTest.jar [Simpex Username] [Simpex Password] [Vehicle Name]\r\n"
					+ "or set this parameters under Run/Debug Arguments."
			);
		}
	}

	public SimpexTest(String username, String password, String vehicle) {
		MessageServiceSoap soap = getSoapService(username, password);
		if (soap != null) {
			// Test getVersion()
			System.out.println("Test Simpex Webservice");
			System.out.println("Simpex Version: " + getVersion(soap));

			// Test Message
			System.out.println("Message result: "
					+ sendTextMessage(soap, vehicle, "Hallo Fahrer! Eine Testnachricht f�r dich. Gru� Dispo"));
			
			// Test Demo Tour
			System.out.println("Tour result: " + sendTourMessage(soap, vehicle));

			// Get MessageCount of Incoming Message Queue
			System.out.println("Incoming Message Count: " + getIncomingMessageCount(soap));

		} else {
			System.out.println("Can't create SOAP Connection.");
		}

	}

	private MessageServiceSoap getSoapService(String username, String password) {
		try {
			// Initialize Soap Webservice
			MessageServiceLocator locator = new MessageServiceLocator();
			MessageServiceSoap soap = locator
					.getMessageServiceSoap(new URL("https://ws2.spedion.de/simpex/3.1/Services/MessageService.asmx"));
			// Using Basic HTTP Authentication
			((Stub) soap)._setProperty(Call.USERNAME_PROPERTY, username);
			((Stub) soap)._setProperty(Call.PASSWORD_PROPERTY, password);
			((Stub) soap)._setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE);
			return soap;
		} catch (MalformedURLException | ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getVersion(MessageServiceSoap soap) {
		try {
			return soap.getVersion();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}

	private String sendTextMessage(MessageServiceSoap soap, String vehicle, String messageText) {
		Message msg = SimpexFactory.createMessage();
		msg.setVehicle(vehicle);
		msg.setText(messageText);
		msg.setForm(1501);
		try {
			Result res = soap.addMessage(msg);
			return res.getCode().getValue();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}

	private String sendTourMessage(MessageServiceSoap soap, String vehicle) {
		Message msg = SimpexFactory.createMessage();
		msg.setVehicle(vehicle);
		msg.setTour(SimpexFactory.createDemoTour());
		msg.setForm(1330);
		try {
			Result res = soap.addMessage(msg);
			return res.getCode().getValue();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}

	private int getIncomingMessageCount(MessageServiceSoap soap) {
		try {
			return soap.getMessageCount();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return -1;
	}

}


